<?php
include_once("./function.php");
$quizdata = returnQuizdata();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
<meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" type="text/css">

<?php
include_once("./function.php");
?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<title><?php echo KENTEI_NAME;?></title>
</head>

<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php"><?php echo KENTEI_NAME;?></a></h1>
  </div>
  <div id="contents">
  	<h2>合否判定<br /><span id="total">さあ、答え合わせです。各問題に関する解説も一緒に載せているので読んでみてくださいね。</span></h2>
		<div class="spacer20">&nbsp;</div>
    
    <?php
    	getResult($quizdata);
      $missNum = 0;

      if($missNum <= MIS_AMOUNT){
      foreach($_POST[q] as $key => $val ){
      if($quizdata[$key-1][2] != $val ){
          $missNum++;
        }
      }
      $correct_Num = QUIZAMOUNT-$missNum;

      //終了時間取得
      $start_time = htmlspecialchars($_POST['start_time']);
      $time = time() - date("Z");  //ローカル時刻にGMTとの時差を引く
      $time = $time + 9*3600;  //ついでに日本時間を表示
      $end_time = date("Y/m/d H:i:s", $time);

echo <<<EOT
<div id="mailForm">
<form method="post" id="mailFormA" name="mailFormA" action="mail.php">
<input type="hidden" name="correct_Num" value="{$correct_Num}">
<input type="hidden" name="start_time" value="{$start_time}">
<input type="hidden" name="end_time" value="{$end_time}">
<div class="quizArea">
<p class="lead">・名前入りの認定証を発行致しますのでお名前・メールアドレスをご入力下さい。<br />・旧字や第二水準以外の漢字は表示できない場合があります。表示がおかしい場合はご連絡ください。<br />・送信後に認定証が画面表示されると同時に記入頂いたメールアドレス宛に同じ画像が添付ファイルにて送信されます。<br />・現在試験的に名刺サイズにプリントされた認定証を郵送するサービスを行っております。希望される方は送付先住所までご記入下さい。</p>
<table style="margin:15px 0;" id="mailTable">
  <tr><th><strong>お名前<strong></th>
  <td><input type="text" id="f_name" name="f_name" maxlength="10" /></td></tr>
  <tr><td><div class="spacer5">&nbsp;</div></td></tr>
  <tr><th><strong>メールアドレス<strong></th>
  <td><input type="text" id="f_mail" name="f_mail" style="width: 300px;" maxlength="40" /></td></tr>
  <tr><td><div class="spacer5">&nbsp;</div></td></tr>
  <tr><th><strong>認定証郵送を</strong></th>
  <td><label><input type="radio" id="f_send" name="f_send" value="希望する" checked />希望する</label> / <label><input type="radio" id="f_send" name="f_send" value="希望しない" />希望しない</label></td></tr>
  <tr><td><div class="spacer5">&nbsp;</div></td></tr>
  <tr><th><strong>郵便番号</strong></th>
  <td><input id="postcode" name="postcode" style="width:100px;"></td></tr>
  <tr><td><div class="spacer5">&nbsp;</div></td></tr>
  <tr><th><strong>都道府県</strong></th>
  <td><select id="address1" name="address1">
  <option value="">選択して下さい</option>
  <option value="北海道">北海道</option><option value="青森県">青森県</option><option value="岩手県">岩手県</option><option value="宮城県">宮城県</option><option value="秋田県">秋田県</option><option value="山形県">山形県</option><option value="福島県">福島県</option><option value="茨城県">茨城県</option><option value="栃木県">栃木県</option><option value="群馬県">群馬県</option><option value="埼玉県">埼玉県</option><option value="千葉県">千葉県</option><option value="東京都">東京都</option><option value="神奈川県">神奈川県</option><option value="新潟県">新潟県</option><option value="富山県">富山県</option><option value="石川県">石川県</option><option value="福井県">福井県</option><option value="山梨県">山梨県</option><option value="長野県">長野県</option><option value="岐阜県">岐阜県</option><option value="静岡県">静岡県</option><option value="愛知県">愛知県</option><option value="三重県">三重県</option><option value="滋賀県">滋賀県</option><option value="京都府">京都府</option><option value="大阪府">大阪府</option><option value="兵庫県">兵庫県</option><option value="奈良県">奈良県</option><option value="和歌山県">和歌山県</option><option value="鳥取県">鳥取県</option><option value="島根県">島根県</option><option value="岡山県">岡山県</option><option value="広島県">広島県</option><option value="山口県">山口県</option><option value="徳島県">徳島県</option><option value="香川県">香川県</option><option value="愛媛県">愛媛県</option><option value="高知県">高知県</option><option value="福岡県">福岡県</option><option value="佐賀県">佐賀県</option><option value="長崎県">長崎県</option><option value="熊本県">熊本県</option><option value="大分県">大分県</option><option value="宮崎県">宮崎県</option><option value="鹿児島県">鹿児島県</option><option value="沖縄県">沖縄県</option></select></td></tr>
  <tr><td><div class="spacer5">&nbsp;</div></tr></td>
  <tr><th><strong>市区町村～番地</strong></th>
  <td><input id="address2" name="address2" style="width: 400px;" maxlength="50" /></td></tr>
</table>
<div class="spacer20" colspan="2">&nbsp;</div>
<p class="small6" style="margin-left: 20px;"><strong>【注意事項】</strong><br />
※携帯電話のメールアドレスの場合、添付ファイルによる認定証が届かない場合があるためPC用のものを推奨します。<br />
※メールアドレスが不達となった場合は郵送を希望されている場合でも郵送されません。ご注意下さい。<br />
※数時間経ってもメールが届かない場合はスパムフィルターに引っかかっているか、入力ミスの可能性が高いです。<br />
※検定は何度でもチャレンジ可能ですが、フォームを連続送信された場合は最初の１枚のみ郵送します。<br />
※認定証の再発行は現在行っておりません。改めて検定にチャレンジ頂くことで再発行が可能です。<br />
※認定証に関するお問い合わせは info_lovefood@lovefood.jp までお願い致します（現在メールのみ対応です）。</p>
</div>
<div class="spacer20">&nbsp;</div>
<a id="submitBtn" class="btn formSend" href='javascript:void(0);'>上記内容で送信する</a>
</form>
</div>
<div class="spacer60">&nbsp;</div>
EOT;
}
?> 
  </div>
 
</div>
<script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="//jpostal.googlecode.com/svn/trunk/jquery.jpostal.js"></script>

<script>
$(window).ready( function() {
	$('#postcode1').jpostal({
		postcode : [
			'#postcode',
		],
		address : {
			'#address1'  : '%3',
			'#address2'  : '%4%5',
		}
	});
});
</script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>