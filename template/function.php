<?php

$quizinfo  = array();
$file = 'csv/quizinfo.csv';
$fp   = fopen($file, "r");
 
while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
	$quizinfo[] = $data;
}
unset($quizinfo[0]);
$quizinfo = array_merge($quizinfo);
define("QUIZAMOUNT" , $quizinfo[0][0]);
define("CORRECT_AMOUNT", $quizinfo[0][1]);
define("KENTEI_NAME", $quizinfo[0][2]);
define("MIS_AMOUNT", $quizinfo[0][0] - $quizinfo[0][1]);
fclose($fp);

/*
	quizdata.csv(問題文と選択肢)を読み込んで
	配列にして返す
*/
function returnQuizdata(){
	$quizdata  = array();
	$file = 'csv/quizdata.csv';
	$fp   = fopen($file, "r");
	 
	while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
	  $quizdata[] = $data;
	}
	unset($quizdata[0]);
	$quizdata = array_merge($quizdata);
	fclose($fp);
	return $quizdata;
}

/*
	クイズデータと問題表示数を渡したら
	問題表示数分ランダムにクイズデータを抽出して
	配列にして返す		
*/
function extractQuiz($quizdata){
	$rand_keys = array_rand($quizdata, QUIZAMOUNT);
	shuffle($rand_keys);
	foreach ($rand_keys as $quizdata_key) {
		$quiz[] = $quizdata[$quizdata_key];
	}
	return $quiz;
}

/*
	クイズデータを渡して
	選択肢をランダムにして
*/
function returnQuizHtml($quiz){
	$quizHtml = "";

	foreach ($quiz as $key => $value) {
		/*
			選択肢の乱数発生
		*/
		$choices = range(2,5);
		shuffle($choices);

		$key++;
		$dispImg = checkImg($value[0]);
		$quizHtml .= <<<EOT
		<div id="Q{$key}">
	      <div class="quizArea">
	      	<table id="quizreslt">
				<tr>
					<td><img src="{$dispImg}" /></td>
					<td><div class="quizText">{$value[1]}</div></td>
				</tr>
			</table>
	        <div class="answerText">
						<table>
							<tr>
								<th><p class="text-red2">A. </p></th><td><p class="text-choices">{$value[$choices[0]]}</p></td>
								<th><p class="text-red2">B. </p></th><td><p class="text-choices">{$value[$choices[1]]}</p></td>
							</tr>
							<tr>
								<th><p class="text-red2">C. </p></th><td><p class="text-choices">{$value[$choices[2]]}</p></td>
								<th><p class="text-red2">D. </p></th><td><p class="text-choices">{$value[$choices[3]]}</p></td>
							</tr>
						</table>
						<div class="spacer20"></div>
	        </div>
	      </div>
				<input type="hidden" name="q[{$value[0]}]" id="answer_{$key}" />
	      <div class="answerBtn" class="btn">
	      	<a href="javascript:void(0);" data-answer="{$value[$choices[0]]}" data-target="#answer_{$key}">A</a>
	      	<a href="javascript:void(0);" data-answer="{$value[$choices[1]]}" data-target="#answer_{$key}">B</a>
	      	<a href="javascript:void(0);" data-answer="{$value[$choices[2]]}" data-target="#answer_{$key}">C</a>
	      	<a href="javascript:void(0);" data-answer="{$value[$choices[3]]}" data-target="#answer_{$key}">D</a>
	      </div>
	    </div>
EOT;
	}	
	return $quizHtml;
}

function getResult($quizdata){
		$missNum = 0; $co=0;
		foreach($_POST[q] as $key => $value ){
			$key--; $co++;
			if($quizdata[$key][2] != $value ){
				$missNum++;
			}
		}
		$correct_Num = QUIZAMOUNT - $missNum;

		if($missNum <= MIS_AMOUNT){
			if( $missNum == 0 ){
				$btn = "<div id='submit2'><p class='text-center nomargin'>全問正解で合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' class='mailFormStart btn' href='javascript:void(0);'>認定証入力フォームへ</a></div><div class='spacer20'>&nbsp;</div>";
			}else{
				$btn = "<div id='submit2'><p class='text-center nomargin'>".QUIZAMOUNT."問中".($correct_Num)."問正解で合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4'  class='mailFormStart btn' href='javascript:void(0);'>認定証入力フォームへ</a></div><div class='spacer20'>&nbsp;</div>";
			}
		}else {
			$btn = "<div id='submit2'><p class='text-center nomargin'>解説を読んで日本酒に少し詳しくなったら再チャレンジしましょう！</p>
							<a id='submitBtn' class='btn' href='quiz.php'>再チャレンジする</a></div><div class='spacer20'>&nbsp;</div>";
		}
		echo $btn;

		foreach($_POST[q] as $key => $value ){
			//不正解数カウント
			if($quizdata[$key-1][2] != $value ){
				$result = "<strong class='large2 text-blue'>　⇒残念！！</strong>";
			}
			else {
				$result = "<strong class='large2 text-red'>　⇒正解！！</strong>";
			}

		$dispImg = checkImg($key++);
			echo <<<EOT
				<div>
					<div class="quizArea">
					<table id="quizreslt">
						<tr>
							<td><img src="$dispImg" /></td>
							<td><div class="quizText">{$quizdata[$key-2][1]}</div></td>
						</tr>
					</table>
					<div class="spacer20">&nbsp;</div>
					<div class="Hantei">
						<strong>[あなたの回答]</strong>
						<p>{$value}{$result}</p>
						<strong>[ただしい回答]</strong>
						<p>{$quizdata[$key-2][2]}</p>
					</div>
					<div class="spacer15">&nbsp;</div>
					<div>
						<strong>[この問題の解説]</strong>
						<p>{$quizdata[$key-2][6]}</p>
					</div>
					<div class="spacer20">&nbsp;</div>
					</div>
				</div>
EOT;
		} 

		if($missNum <= MIS_AMOUNT){
			if( $missNum == 0 ){
				$btn = "<p class='text-center nomargin'>全問正解で合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' class='mailFormStart btn' href='javascript:void(0);'>認定証入力フォームへ</a>";
			}else{
				$btn = "<p class='text-center nomargin'>".QUIZAMOUNT."問中".(QUIZAMOUNT-$missNum)."問クリアで合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' class='mailFormStart btn' href='javascript:void(0);'> 認定証入力フォームへ</a>";
			}
		}
		else {
			$btn = "<p class='text-center nomargin'>解説を読んで日本酒に少し詳しくなったら再チャレンジしましょう！</p>
							<a id='submitBtn' class='btn' href='quiz.php'>再チャレンジする</a>";
		}
			echo <<<EOT
				<div id="submit2">
					{$btn}
				</div>
				<div class="spacer20">&nbsp;</div>
EOT;
}


//#######認定証の生成############
function createNinteisho($name,$correct_amount,$start_time,$end_time){
	global $imageID;

	//MySQLに接続
	$link = mysql_connect('mysql478.db.sakura.ne.jp', 'dsn4', 'adhea9t34oaimss');
	if (!$link) {
			die('接続失敗です。'.mysql_error());
	}
	
	//データベース選択
	$db_selected = mysql_select_db('dsn4_kentei', $link);
	if (!$db_selected){
			die('データベース選択失敗です。'.mysql_error());
	}
	
	mysql_query('SET NAMES utf8');
	
	
	//IDを取得
	$display = mysql_query('
	SELECT *
	FROM `kentei_id`
	where `kentei_id`= 1
	order by `user_id` desc limit 1
	');
		
	if (!$display) {
		die('SELECTクエリーが失敗しました。'.mysql_error());
	}
	
	while ($row = mysql_fetch_assoc($display)) {
		$id = $row[user_id];
	}

	
	$agent = $ENV{'HTTP_USER_AGENT'};
	
	// ファイルパスを指定
	$file = "./test.jpg";
	$file2 = "./test.jpg";

	// jpg画像を作成
	$img = imagecreatefromjpeg( $file );
	$img2 = imagecreatefromjpeg( $file2 );

	// 画像サイズ取得
	$target = getimagesize( $file );
	$target2 = getimagesize( $file2 );

	// フォントサイズ
	$fsize = 36;
	$fsize2 = 56;

	// フォント角度
	$fangle = 0;

	// 位置
	$fx = 445;
	$fy = $fsize + 530;
	$fx2 = 440;
	$fy2 = $fsize2 + 520;

	// フォントカラー
	$fcolor = imagecolorallocate( $img, 0, 0, 0 );
	
	// フォント
	$fpath = "./../ipamp.ttf";
	$numFpath = "./../mplus-1mn-regular.ttf";

	//表示用に最終IDに1を足してカレントのユーザーIDにする
	$id++;
	// テキスト
	// 文字コードは『UTF-8』に変換する
	$ftext = mb_convert_encoding( $name, "UTF-8", "auto" );
	$fID = mb_convert_encoding( $id, "UTF-8", "auto" );
	$fID = 'No.'.sprintf("%04d", $fID);
	$imageID = $fID;
	
	// 表示用ファイル名(こちらは保存する)
	$filename = uniqid( $imageID . "_" );

	// 名前挿入(表示用)
	imagettftext(
		$img,
		$fsize2,
		$fangle,
		$fx+240,
		$fy,
		$fcolor,
		$fpath,
		$ftext
	);

	// ID挿入(表示用)
	imagettftext(
		$img,
		$fsize,
		$fangle,
		$fx,
		$fy,
		$fcolor,
		$numFpath,
		$fID
	);

	// 名前挿入(送信用)
	imagettftext(
		$img2,
		$fsize2,
		$fangle,
		$fx2+240,
		$fy2,
		$fcolor,
		$fpath,
		$ftext
	);

	// ID挿入(送信用)
	imagettftext(
		$img2,
		$fsize,
		$fangle,
		$fx2,
		$fy2,
		$fcolor,
		$numFpath,
		$fID
	);

	// 保存
	// 最後の90は品質：デフォルトは75との事
	imagejpeg( $img, "../ninteisho/" . $filename . ".jpg", 90 );
	imagejpeg( $img2, "../ninteisho/" . $imageID . ".jpg", 90 );
	
	echo <<<EOF
	<p>下記と同じ認定証がメール添付にて送信されましたのでご確認下さい。<br />数時間してもメールが届かない場合はメールアドレスが間違っているか、迷惑メールフィルターに引っかかっている可能性があります。すぐにメール到着が確認できない場合は念のため以下の画像を保存することをお奨めします。</p>
	<img src='../ninteisho/{$filename}.jpg' width='768' id="ninteishoImg"><br clear='all'>
	<img src='./images/quiz/usecase.jpg' id="usecaseImg"><br clear='all'>
EOF;
	imagedestroy( $img );
	
	//ログ保存
	$agent = $_SERVER['HTTP_USER_AGENT'];
	
	$sql3 = "
		INSERT INTO `kentei_id` (`id`, `kentei_id`, `user_id`, `correct_amount`, `useragent`, `start_time`, `end_time` , `datetime`)
		VALUES (NULL, 1 , '$id' , '$correct_amount' , '$agent' ,'$start_time' , '$end_time' , now())";
	$result_flag3 = mysql_query($sql3);
	if (!$result_flag3) {
		die('3.SELECTクエリーが失敗しました。'.mysql_error());
	}
	
	//MySQL切断
	$close_flag = mysql_close($link);
}


//ファイルの有無を調べる
function checkImg($key){
	$filename = "images/quiz/q".$key.".jpg";
	if ( file_exists( $filename )) {
		return $dispImg = $filename;
	} else {
		return $dispImg = "images/quiz/6.jpg";
	}
}

/*
	合格者と管理者にメール送信
*/
function sendMail($to,$name,$zip,$pref,$addr,$f_send,$correct,$start,$end){
	global $imageID;
	
// $from = "info_lovefood@lovefood.jp";
//// $bcc = "asai@thedesignium.com";
// $bcc = "i-takeshi@thedesignium.com";
$from = "s1200029@gmail.com";
// $bcc = "s1200029@gmail.com";

// ######合格者用メール#######
  $subject = "会津日本酒検定認定証発行";
  $body = <<< EOB
  
  以下の内容で認定証発行を受け付けました。
  
  [お名前]
  {$name}
  
  [メールアドレス]
  {$to}
  
  [認定証郵送を]
  {$f_send}
  
  [住所]
  〒 {$zip}
  {$pref}{$addr}
  
  
  以下、会津日本酒検定指南役の心得をお読みになり、
  日本酒の振興に努めて頂けますようお願い申し上げます。

  【指南役の心得】
  　一、日本酒の素晴らしさを周りに丁寧に伝えること
  　一、そのためにまず自分が日本酒を深く知り、楽しむこと
  　一、飲み過ぎないこと、飲ませ過ぎないこと
  　一、指南役仲間を増やし、日本酒の振興に努めること
  
  --------------------------------------------------------
  　会津日本酒検定　http://lovefood.jp/sake/
  　～ 一杯からの日本酒指南で美味しい酒を飲む仲間を増やそう ～
  
  　問題提供/監修：会津若松酒造協同組合、福島県ハイテクプラザ
  　企画協力：力水會、lovefood Project
  　デザイン/開発：株式会社デザイニウム
  　お問い合わせ先：info_lovefood@lovefood.jp
  --------------------------------------------------------
EOB;

  //バウンダリー文字(パートの境界)
  $boundary = "_Boundary_" . uniqid(rand(1000,9999) . '_') . "_"; 
  
  //Subject部分を変換
  $xSubject = mb_convert_encoding($subject, "ISO-2022-JP", "auto");
  $xSubject = base64_encode($xSubject);
  $xSubject = "=?iso-2022-jp?B?".$xSubject."?=";
  
  //Message部分を変換
  $xMessage = htmlspecialchars($body);
  $xMessage = str_replace("&amp;", "&", $xMessage);
  if (get_magic_quotes_gpc()) $xMessage = stripslashes($xMessage);
  $xMessage = str_replace("\r\n", "\r", $xMessage);
  $xMessage = str_replace("\r", "\n", $xMessage);
  $xMessage = mb_convert_encoding($xMessage, "ISO-2022-JP", "auto");

  // Header部分を生成
  $GMT = date("Z");
  $GMT_ABS  = abs($GMT);
  $GMT_HOUR = floor($GMT_ABS / 3600);
  $GMT_MIN = floor(($GMT_ABS - $GMT_HOUR * 3600) / 60);
  if ($GMT >= 0) $GMT_FLG = "+"; else $GMT_FLG = "-";
  $GMT_RFC = date("D, d M Y H:i:s ").sprintf($GMT_FLG."%02d%02d",$GMT_HOUR, $GMT_MIN);
  
  $Headers  = "To: $to\n" .
           "Bcc: $bcc\n" .
           "Date: ".$GMT_RFC."\n" .
           "From: $from\n" .
           "X-Mailer: PHP/" . phpversion() . "\n" .
           "MIME-Version: 1.0\n" .
           "Content-Type: Multipart/Mixed; boundary=\"$boundary\"\n" .
           "Content-Transfer-Encoding: 7bit";

  // 添付データのエンコード
  // 日本語のファイル名はRFC違反ですが、多くのメーラは理解します
  $filename = mb_encode_mimeheader( $imageID . ".jpg" );           // ISO-2022-JP/Base64に変換
  ($attach = file_get_contents("../ninteisho/" . $imageID . ".jpg")) Or die("Open Error: $filename");
  $attach   = chunk_split(base64_encode($attach),76,"\n"); // Base64に変換し76Byte分割
  
  //ファイル添付がある場合
  $body    = mb_convert_encoding($body, 'ISO-2022-JP', 'auto'); // ISO-2022-JPに変換
  // マルチパート:本文
  $mbody .= "--$boundary\n";
  $mbody .= "Content-Type: text/plain; charset=ISO-2022-JP\n" .
            "Content-Transfer-Encoding: 7bit\n";
  $mbody .= "\n";        // 空行
  $mbody .= "$body\n";   // 本文

  // マルチパート:添付ファイル
  $mbody .= "--$boundary\n";
  $mbody .= "Content-Type: $mime; name=\"$filename\"\n" .
            "Content-Transfer-Encoding: base64\n" .
            "Content-Disposition: attachment; filename=\"$filename\"\n";
  $mbody .= "\n";        // 空行
  $mbody .= "$attach\n"; // 添付

  // マルチパート:終わり
  $mbody .= "--$boundary--\n";
  
  $Headers .= "Content-type: text/plain; charset=utf-8\n";
  $Headers .= "Content-Transfer-Encoding: 7bit";

  //スタッフ用メール
  $to_staff = "sakai051669@gmail.com";
  $s_from = "s1200029@gmail.com";
// $s_bcc = "s1200029@gmail.com";

  $agent = $ENV{'HTTP_USER_AGENT'};
  $agent = $_SERVER['HTTP_USER_AGENT'];

  $s_subject = "会津日本酒検定認定証発行";
  $s_body = <<< EOB
  
  以下の内容で認定証発行を受け付けました。
  
  [お名前]
  {$name}
  
  [メールアドレス]
  {$to}
  
  [認定証郵送を]
  {$f_send}
  
  [住所]
  〒 {$zip}
  {$pref}{$addr}
  
  
  [ブラウザ・端末情報]
  {$agent}

   [開始・終了時間]
  開始時間：{$start}
  終了時間：{$end}
 
  --------------------------------------------------------
  　会津日本酒検定　http://lovefood.jp/sake/
  　～ 一杯からの日本酒指南で美味しい酒を飲む仲間を増やそう ～
  
  　問題提供/監修：会津若松酒造協同組合、福島県ハイテクプラザ
  　企画協力：力水會、lovefood Project
  　デザイン/開発：株式会社デザイニウム
  　お問い合わせ先：info_lovefood@lovefood.jp
  --------------------------------------------------------
EOB;

  //バウンダリー文字(パートの境界)：スタッフ
  $s_boundary = "_Boundary_" . uniqid(rand(1000,9999) . '_') . "_"; 
  
  //Subject部分を変換：スタッフ
  $s_xSubject = mb_convert_encoding($s_subject, "ISO-2022-JP", "auto");
  $s_xSubject = base64_encode($s_xSubject);
  $s_xSubject = "=?iso-2022-jp?B?".$s_xSubject."?=";
  
  //Message部分を変換：スタッフ
  $s_xMessage = htmlspecialchars($s_body);
  $s_xMessage = str_replace("&amp;", "&", $s_xMessage);
  if (get_magic_quotes_gpc()) $s_xMessage = stripslashes($s_xMessage);
  $s_xMessage = str_replace("\r\n", "\r", $s_xMessage);
  $s_xMessage = str_replace("\r", "\n", $s_xMessage);
  $s_xMessage = mb_convert_encoding($s_xMessage, "ISO-2022-JP", "auto");
 
   // Header部分を生成
  $s_GMT = date("Z");
  $s_GMT_ABS  = abs($s_GMT);
  $s_GMT_HOUR = floor($s_GMT_ABS / 3600);
  $s_GMT_MIN = floor(($s_GMT_ABS - $s_GMT_HOUR * 3600) / 60);
  if ($s_GMT >= 0) $s_GMT_FLG = "+"; else $s_GMT_FLG = "-";
  $s_GMT_RFC = date("D, d M Y H:i:s ").sprintf($s_GMT_FLG."%02d%02d",$s_GMT_HOUR, $s_GMT_MIN);
  
  $s_Headers  = "To: $to_staff\n" .
           "Bcc: $s_bcc\n" .
           "Date: ".$s_GMT_RFC."\n" .
           "From: $s_from\n" .
           "X-Mailer: PHP/" . phpversion() . "\n" .
           "MIME-Version: 1.0\n" .
           "Content-Type: Multipart/Mixed; boundary=\"$s_boundary\"\n" .
           "Content-Transfer-Encoding: 7bit";
  
  //ファイル添付がある場合
  $s_body    = mb_convert_encoding($s_body, 'ISO-2022-JP', 'auto'); // ISO-2022-JPに変換
  // マルチパート:本文
  $s_mbody .= "--$s_boundary\n";
  $s_mbody .= "Content-Type: text/plain; charset=ISO-2022-JP\n" .
            "Content-Transfer-Encoding: 7bit\n";
  $s_mbody .= "\n";        // 空行
  $s_mbody .= "$s_body\n";   // 本文

  // マルチパート:添付ファイル
  $s_mbody .= "--$s_boundary\n";
  $s_mbody .= "Content-Type: $s_mime; name=\"$filename\"\n" .
            "Content-Transfer-Encoding: base64\n" .
            "Content-Disposition: attachment; filename=\"$filename\"\n";
  $s_mbody .= "\n";        // 空行
  $s_mbody .= "$attach\n"; // 添付

  // マルチパート:終わり
  $s_mbody .= "--$s_boundary--\n";
  
  $s_Headers .= "Content-type: text/plain; charset=utf-8\n";
  $s_Headers .= "Content-Transfer-Encoding: 7bit";

  //メール送信 終了報告
  if( mail($to,$xSubject,$mbody,$Headers) && mail($to_staff,$s_xSubject,$s_mbody,$s_Headers) ){
    unlink("../ninteisho/" . $imageID . ".jpg");
    echo <<<EOT
      <!--<p class="padding18">応募が完了しました。<br>入力したアドレスへ確認メールが届きますのでご確認下さい。</p>-->
EOT;
  }
	else{
    echo <<<EOT
      <p class="padding18">申し訳ございません。メールの送信に失敗しました。時間をおいて再度やり直していただくようお願いいたします。</p>
EOT;
  }

}

?>

