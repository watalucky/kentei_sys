var QuizAmount = $("#QuizAmount").text();

$(".answerBtn a").bind('touchstart', function(){
		$(this).parent().children("a.current").removeClass("current");
		$(this).addClass("current");
		
		var target = $(this).attr("data-target");
		var answer = $(this).attr("data-answer");
		$(target).val(answer);
	}
);
$(".answerBtn a").click(function(){
		$(this).parent().children("a.current").removeClass("current");
		$(this).addClass("current");
		
		var target = $(this).attr("data-target");
		var answer = $(this).attr("data-answer");
		$(target).val(answer);
	}
);

$("#next").click(function(){
		var qNo = Number($("#nowQuestion").val());
		$("#prev").show();
		
		if(qNo != QuizAmount ){
			if(qNo == QuizAmount - 1){
				$("#next").hide();
				$("#submit").show();
			}
			var nextqNo = qNo + 1;
			$("#Q"+qNo).hide();
			btnPush(nextqNo);
		}
	}
);

$("#prev").click(function(){
		var qNo = Number($("#nowQuestion").val());
		$("#next").show();
		$("#submit").hide();
		
		if(qNo != 1){
			if(qNo == 2){
				$("#prev").hide();
			}
			var nextqNo = qNo - 1;
			$("#Q"+qNo).hide();
			btnPush(nextqNo);
		}
	}
);

$(".mailFormStart").click(function(){
		$("#contents *").hide();
		$("#contents h2").html("認定証入力フォーム<br /><div class='spacer20'>&nbsp;</div>");
		$("#contents h2").show();
		$("#mailForm").show();
		$("#mailForm *").show();
});

$(".formSend").click(function () {
	if( $("input[name=f_send]:checked").val() && $("#f_name").val()  && $("#f_mail").val() ){
		if( $("input[name=f_send]:checked").val() == "希望する" ){
			//証明書発送を希望する場合は住所入力必須
			if( $("#postcode").val() && $("#address1").val() && $("#address2").val() && $("#f_mail").val()){
				if (! $("#f_mail").val().match(/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/)) {
					alert("正しいメールアドレスを入力してください。")
				}else{
					document.mailFormA.submit();
				}
			}
			else{
				alert("認定証の発送を希望される場合、住所入力は必須です。");
			}
		}
		else{
			//認定証の発送を希望しない場合はそのまま送信
			document.mailFormA.submit();
		}
	}
	else{
		alert("未入力の項目があります。");
	}
});

function btnPush(no){
		$("#no").html(no);
		$("#Q"+no).show();
		$("#nowQuestion").val(no);
}

$(function(){
	if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
		$("body p").addClass("large4");
		$("body table").addClass("large4");
	}
});





















