<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
<meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" type="text/css">

<?php
include_once("./function.php");
?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<title><?php echo $quizinfo[1][2];?></title>
</head>

<body id="mail">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php"><?php echo KENTEI_NAME;?></a></h1>
  </div>
  <div id="contents">
    <h2>指南役認定おめでとうございます。<br /><span id="message">豊かな地形と名水に恵まれた、会津で生まれた極上の日本酒<br />一杯からの日本酒指南でおいしく楽しく飲む仲間を増やしていこう！</span></h2>
    <div class="spacer20">&nbsp;</div>
    <div class="quizArea">
      <?php
      $to = htmlspecialchars($_POST['f_mail']);
      $name = htmlspecialchars($_POST['f_name']);
      $zip = htmlspecialchars($_POST['postcode']);
      $pref = htmlspecialchars($_POST['address1']);
      $addr = htmlspecialchars($_POST['address2']);
      $f_send = htmlspecialchars($_POST['f_send']);
      $correct = htmlspecialchars($_POST['correct_Num']);
      $start = htmlspecialchars($_POST['start_time']);
      $end = htmlspecialchars($_POST['end_time']);

      //認定証の生成
      createNinteisho($_POST['f_name'],$_POST['correct_Num'],$_POST['start_time'],$_POST['end_time']);
      sendMail($to,$name,$zip,$pref,$addr,$f_send,$correct,$start,$end);
      ?>

    </div>
    <div class="spacer20">&nbsp;</div>
  </div>
</div>
<script src="http://www.google.com/jsapi"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.min.js"></script>

<script type="text/javascript" src="js/script.js"></script>
<script src="./js/ajaxzip2/ajaxzip2.js" charset="UTF-8"></script>
<script type='text/javascript' src='http://code.jquery.com/jquery-git2.js'></script>
<script type="text/javascript" src="http://jpostal.googlecode.com/svn/trunk/jquery.jpostal.js"></script>
</body>
</html>