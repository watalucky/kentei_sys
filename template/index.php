<?php
include_once("./function.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
<meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">
 
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="http://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
 <title><?php echo KENTEI_NAME;?></title>
</head>

<body id="top">	
	<div id="wrapper" class="top_wrapper">
		<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Flovefood.jp%2Fsake%2Fpc%2F&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:120px; height:21px; float:right;" allowTransparency="true"></iframe>
		
		<h1><?php echo KENTEI_NAME;?></h1>
		<h2>検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。一杯からの日本酒指南でおいしい酒を飲む仲間を増やそう！</h2>
		
		<div id="goukakugo">
			<ol type="decima">
				<li>約40問から10問がランダムに出題されますので、正答率 80%を目指して頑張って下さい！</li>
				<li>正答率80%を越えると認定証が発行されます。郵送も可能ですし電子データの保存も可能です。</li>
				<li>見事日本酒指南役認定を受けた方は是非お酒の席で見せびらかし酒談義に花を咲かせましょう。</li>
				<li>日本酒の消費を拡大してお酒と仲間を増やしましょう！</li>
			</ol>
		</div>

		<img src="./images/top/ninteisyo.jpg" alt="ninteisyo" id="ninteisyo" />

		<div class="imgBox" class="spNone">
			<img src="./images/mbile/sample.png" alt="sample" id="sample"/>
			<img src="./images/mbile/shinankokoroe.png" alt="kokoroe" id="kokoroe"/>
		</div>

		<div id="btnArea">
			<h5>\ new /</h5>
			<a href="service.php" id="serviceBtn" class="btn">認定証でサービスが受けられる！店舗リスト</a>	
			<a href="quiz.php" id="submitBtn" class="btn" onclick="getTimestamp()"><?php echo KENTEI_NAME;?>に挑戦する！</a>
		</div>

		<div id="footer">
			<p>Copyright &copy; Aizuwakamatsu Brewery Co-operative + Rikisuikai + TheDesignium Inc.</p>
		</div>
	</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
