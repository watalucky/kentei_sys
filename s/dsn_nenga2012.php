<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<title>福島＆会津クイズ presented by The Designium</title>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">福島&amp;会津クイズ presented by The Designium</a></h1>
  </div>
  <div id="contents">
  	<h2>問題<span id="no">1</span><br /><span id="total">計6題</span></h2>
    <div class="spacer20">&nbsp;</div>
    <form name="quizForm" action="./result2012.php" method="post">
    
    <?php
		quizRand();
		?>
    
    <div id="controller">
    	<a href="javascript:void(0);" id="prev">&lt;&lt;前へ</a>
    	<a href="javascript:void(0);" id="next">次へ&gt;&gt;</a>
    </div>
    
    <div id="submit">
    	<input type="submit" value="答え合わせをする" id="submitBtn" />
    </div>
    
    </form>
    <input type="hidden" value="1" id="nowQuestion" name="nowQuestion" />
    <div class="spacer60">&nbsp;</div>
    
  </div>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
