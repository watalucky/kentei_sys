<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<title>福島＆会津クイズ presented by The Designium</title>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<?php

/*print_r($_POST);
print_r($CorrectAnswer);*/
?>
<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">福島&amp;会津クイズ presented by The Designium</a></h1>
  </div>
  <div id="contents">
  	<h2>答え合わせ<br /><span id="total">さあ、答え合わせです。各問題に関する豆知識も一緒に載せているので読んでみてくださいね。</span></h2>
		<div class="spacer20">&nbsp;</div>
    
    <?php
		$missNum = 0;
		foreach($_POST as $key => $val ){
			if($CorrectAnswer[$key] != $val ){
				$missNum++;
			}
		}
		if($missNum == 0){
			$btn = "<div id='submit2'><p class='text-center nomargin text-orange'>全問正解おめでとうございます！<br>商品応募ページへ進んで下さい。</p>
							<a id='submitBtn4' href='javascript:void(0);' class='mailFormStart'> 商品応募ページへ</a></div><div class='spacer20'>&nbsp;</div>";
			echo $btn;
		}
		foreach($_POST as $key => $val ){
			//不正解数カウント
			if($CorrectAnswer[$key] != $val ){
				echo $CorrectAnswer[$key]. ":" . $val;
				$result = "<strong class='large3 text-red'>　⇒残念！！</strong>";
			}
			else {
				$result = "<strong class='large3 text-blue'>　⇒正解！！</strong>";
			}
			echo <<<EOT
				<div>
					<div class="quizArea">
						<img src="images/quiz/{$key}.jpg" />
						<div class="quizText large4">
							{$question[$key]}
						</div>
					<div class="spacer20">&nbsp;</div>
					<table class="large3">
						<tr><th>[あなたの回答]　</th><td>{$val}</td><td rowspan="2">{$result}</td></tr>
						<tr><th>[ただしい回答]　</th><td>{$CorrectAnswer[$key]}</td></tr>
					</table>
					<div class="spacer15">&nbsp;</div>
					<table class="large3">
						<tr><th style="width:190px;">[豆知識！]　</th><td>{$mame[$key]}</td></tr>
					</table>
					<div class="spacer20">&nbsp;</div>
					</div>
				</div>
				<div class="spacer20">&nbsp;</div>
EOT;
		} 
		if($missNum == 0){
			$btn = "<p class='text-center nomargin text-orange'>全問正解おめでとうございます！<br>商品応募ページへ進んで下さい。</p>
							<a id='submitBtn4' href='javascript:void(0);' class='mailFormStart'> 商品応募ページへ</a>";
		}
		else {
			$btn = "<p class='text-center nomargin'>福島と会津について少し詳しくなったなら再チャレンジしてみますか？</p>
							<a id='submitBtn' href='dsn_nenga2012.php'>再チャレンジ</a>";
		}
			echo <<<EOT
				<div id="submit2">
					{$btn}
				</div>
				<div class="spacer20">&nbsp;</div>
EOT;
		?>
    
    
  </div>
  <?php
		if($missNum == 0){
			echo <<<EOT
				<div id="mailForm">
					<form method="post" id="mailFormA" name="mailFormA" action="mail.php">
					<div class="quizArea">
						<table style="margin:15px 0;">
							<tr><th>希望プレゼント</th><td><select name="gift" id="gift"><option value="">選択して下さい</option><option value="日本酒セット">日本酒セット</option><option value="蜂蜜セット">蜂蜜セット</option><option value="野菜セット">野菜セット</option></select>
							<p class="small1">商品詳細は弊社が運営しておりますlovefood.jpに掲載している作り手インタビューをご覧ください。<br />
							日本酒セット(<a href="http://lovefood.jp/fukushima/archive/sake/sake1.php" target="_blank">会津娘純米</a> / <a href="http://lovefood.jp/fukushima/archive/sake/sake7.php" target="_blank">寫楽純米吟醸</a>)｜<a href="http://lovefood.jp/fukushima/interview/report1/" target="_blank">野菜セット</a>｜<a href="http://lovefood.jp/fukushima/interview/report3/" target="_blank">蜂蜜セット</a></p></td></tr>
							<tr><th>お名前</th><td><input type="text" id="f_name" name="f_name" maxlength="20" /></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>メールアドレス</th><td><input type="text" id="f_mail" name="f_mail" style="width: 300px;" maxlength="40" /></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>郵便番号</th><td><input type="text" id="zip" name="zip" onKeyUp="AjaxZip2.zip2addr(this,'pref','addr');" style="width:100px;"></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>都道府県</th><td><select id="pref" name="pref">
							<option value="">選択して下さい</option>
							<option value="北海道">北海道</option><option value="青森県">青森県</option><option value="岩手県">岩手県</option><option value="宮城県">宮城県</option><option value="秋田県">秋田県</option><option value="山形県">山形県</option><option value="福島県">福島県</option><option value="茨城県">茨城県</option><option value="栃木県">栃木県</option><option value="群馬県">群馬県</option><option value="埼玉県">埼玉県</option><option value="千葉県">千葉県</option><option value="東京都">東京都</option><option value="神奈川県">神奈川県</option><option value="新潟県">新潟県</option><option value="富山県">富山県</option><option value="石川県">石川県</option><option value="福井県">福井県</option><option value="山梨県">山梨県</option><option value="長野県">長野県</option><option value="岐阜県">岐阜県</option><option value="静岡県">静岡県</option><option value="愛知県">愛知県</option><option value="三重県">三重県</option><option value="滋賀県">滋賀県</option><option value="京都府">京都府</option><option value="大阪府">大阪府</option><option value="兵庫県">兵庫県</option><option value="奈良県">奈良県</option><option value="和歌山県">和歌山県</option><option value="鳥取県">鳥取県</option><option value="島根県">島根県</option><option value="岡山県">岡山県</option><option value="広島県">広島県</option><option value="山口県">山口県</option><option value="徳島県">徳島県</option><option value="香川県">香川県</option><option value="愛媛県">愛媛県</option><option value="高知県">高知県</option><option value="福岡県">福岡県</option><option value="佐賀県">佐賀県</option><option value="長崎県">長崎県</option><option value="熊本県">熊本県</option><option value="大分県">大分県</option><option value="宮崎県">宮崎県</option><option value="鹿児島県">鹿児島県</option><option value="沖縄県">沖縄県</option></select></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>市区町村～番地</th><td><input type="text" id="addr" name="addr" style="width: 500px;" maxlength="50" /></td></tr>
						</table>
					</div>
					<div class="spacer20">&nbsp;</div>
					<div id="submit3">
						<a id='submitBtn' href='javascript:void(0);' class="formSend">上記内容で送信する</a>
						</form>
					</div>
				</div>
				<div class="spacer60">&nbsp;</div>
EOT;
		}
	?>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
<script src="./js/ajaxzip2/ajaxzip2.js" charset="UTF-8"></script>
</body>
</html>