<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
 <meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<title>会津日本酒検定</title>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<?php

/*print_r($_POST);
print_r($CorrectAnswer);*/
?>
<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">会津日本酒検定</a></h1>
  </div>
  <div id="contents">
  	<h2>合否判定<br /><span id="total">さあ、答え合わせです。各問題に関する解説も一緒に載せているので読んでみてくださいね。</span></h2>
		<div class="spacer20">&nbsp;</div>
    
    <?php
		$missNum = 0;
		$questionNum = 0; //問題数
		$allowMissNum = 2; //許容間違い数
		foreach($_POST as $key => $val ){
			$questionNum++;
			if($CorrectAnswer[$key] != $val ){
				$missNum++;
			}
		}
		if($missNum <= $allowMissNum){
			if( $missNum == 0 ){
				$btn = "<div id='submit2'><p class='text-center nomargin'>全問正解で合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' href='javascript:void(0);' class='mailFormStart'>認定証入力フォームへ</a></div><div class='spacer20'>&nbsp;</div>";
			}else{
				$btn = "<div id='submit2'><p class='text-center nomargin'>".$questionNum."問中".($questionNum-$missNum)."問正解で合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' href='javascript:void(0);' class='mailFormStart'>認定証入力フォームへ</a></div><div class='spacer20'>&nbsp;</div>";
			}
		}else {
			$btn = "<div id='submit2'><p class='text-center nomargin'>解説を読んで日本酒に少し詳しくなったら再チャレンジしましょう！</p>
							<a id='submitBtn' href='kentei.php'>再チャレンジする</a></div><div class='spacer20'>&nbsp;</div>";
		}
		echo $btn;
		foreach($_POST as $key => $val ){
			//不正解数カウント
			if($CorrectAnswer[$key] != $val ){
				//echo $CorrectAnswer[$key]. ":" . $val;
				$result = "<strong class='large2 text-blue'>　⇒残念！！</strong>";
			}
			else {
				$result = "<strong class='large2 text-red'>　⇒正解！！</strong>";
			}
			echo <<<EOT
				<div>
					<div class="quizArea">
						<img src="images/quiz/{$key}.jpg" />
						<div class="quizText">
							{$question[$key]}
						</div>
					<div class="spacer20">&nbsp;</div>
					<table class="Hantei">
						<tr><th width="170"><strong>[あなたの回答]</strong>　</th><td width="470">{$val}</td><td width="170" rowspan="2">{$result}</td></tr>
						<tr><th><strong>[ただしい回答]</strong>　</th><td>{$CorrectAnswer[$key]}</td></tr>
					</table>
					<div class="spacer15">&nbsp;</div>
					<table>
						<tr><th style="width:100px;"><strong>[この問題の解説]</strong>　</th><td>{$mame[$key]}</td></tr>
					</table>
					<div class="spacer20">&nbsp;</div>
					</div>
				</div>
				<div class="spacer20">&nbsp;</div>
EOT;
		} 
		if($missNum <= $allowMissNum){
			if( $missNum == 0 ){
				$btn = "<p class='text-center nomargin'>全問正解で合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' href='javascript:void(0);' class='mailFormStart'>認定証入力フォームへ</a>";
			}else{
				$btn = "<p class='text-center nomargin'>".$questionNum."問中".($questionNum-$missNum)."問クリアで合格おめでとうございます！認定証入力フォームへ進んで下さい。</p>
							<a id='submitBtn4' href='javascript:void(0);' class='mailFormStart'> 認定証入力フォームへ</a>";
			}
		}
		else {
			$btn = "<p class='text-center nomargin'>解説を読んで日本酒に少し詳しくなったら再チャレンジしましょう！</p>
							<a id='submitBtn' href='kentei.php'>再チャレンジする</a>";
		}
			echo <<<EOT
				<div id="submit2">
					{$btn}
				</div>
				<div class="spacer20">&nbsp;</div>
EOT;
		?>
    
    
  </div>
  <?php
		if($missNum <= $allowMissNum){
			echo <<<EOT
				<div id="mailForm">
					<form method="post" id="mailFormA" name="mailFormA" action="mail.php">
					<div class="quizArea">
						<p class="lead">・名前入りの認定証を発行致しますのでお名前・メールアドレスをご入力下さい。<br />・旧字や第二水準以外の漢字は表示できない場合があります。表示がおかしい場合はご連絡ください。<br />・送信後に認定証が画面表示されると同時に記入頂いたメールアドレス宛に同じ画像が添付ファイルにて送信されます。<br />・現在試験的に名刺サイズにプリントされた認定証を郵送するサービスを行っております。希望される方は送付先住所までご記入下さい。</p>
						<table style="margin:15px 0;" id="mailTable">
							<tr><th>お名前</th><td><input type="text" id="f_name" name="f_name" maxlength="20" /></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>メールアドレス</th><td><input type="text" id="f_mail" name="f_mail" style="width: 300px;" maxlength="40" /></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>認定証郵送を</th><td><label><input type="radio" id="f_send" name="f_send" value="希望する" checked />希望する</label> / <label><input type="radio" id="f_send" name="f_send" value="希望しない" />希望しない</label></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>郵便番号</th><td><input type="text" id="zip" name="zip" onKeyUp="AjaxZip2.zip2addr(this,'pref','addr');" style="width:100px;"></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>都道府県</th><td><select id="pref" name="pref">
							<option value="">選択して下さい</option>
							<option value="北海道">北海道</option><option value="青森県">青森県</option><option value="岩手県">岩手県</option><option value="宮城県">宮城県</option><option value="秋田県">秋田県</option><option value="山形県">山形県</option><option value="福島県">福島県</option><option value="茨城県">茨城県</option><option value="栃木県">栃木県</option><option value="群馬県">群馬県</option><option value="埼玉県">埼玉県</option><option value="千葉県">千葉県</option><option value="東京都">東京都</option><option value="神奈川県">神奈川県</option><option value="新潟県">新潟県</option><option value="富山県">富山県</option><option value="石川県">石川県</option><option value="福井県">福井県</option><option value="山梨県">山梨県</option><option value="長野県">長野県</option><option value="岐阜県">岐阜県</option><option value="静岡県">静岡県</option><option value="愛知県">愛知県</option><option value="三重県">三重県</option><option value="滋賀県">滋賀県</option><option value="京都府">京都府</option><option value="大阪府">大阪府</option><option value="兵庫県">兵庫県</option><option value="奈良県">奈良県</option><option value="和歌山県">和歌山県</option><option value="鳥取県">鳥取県</option><option value="島根県">島根県</option><option value="岡山県">岡山県</option><option value="広島県">広島県</option><option value="山口県">山口県</option><option value="徳島県">徳島県</option><option value="香川県">香川県</option><option value="愛媛県">愛媛県</option><option value="高知県">高知県</option><option value="福岡県">福岡県</option><option value="佐賀県">佐賀県</option><option value="長崎県">長崎県</option><option value="熊本県">熊本県</option><option value="大分県">大分県</option><option value="宮崎県">宮崎県</option><option value="鹿児島県">鹿児島県</option><option value="沖縄県">沖縄県</option></select></td></tr>
							<tr><td colspan="2"><div class="spacer5">&nbsp;</div></td></tr>
							<tr><th>市区町村～番地</th><td><input type="text" id="addr" name="addr" style="width: 500px;" maxlength="50" /></td></tr>
						</table>
						<div class="spacer20">&nbsp;</div>
						<p class="small1" style="margin-left: 20px;"><strong>【注意事項】</strong><br />
						※携帯電話用のメールアドレスの場合、添付ファイルによる認定証が届かない場合があります。PC用のものをご指定下さい。<br />
						※メールアドレスが不達となった場合は郵送を希望されている場合でも郵送されません。ご注意下さい。<br />
						※複数回の郵送申し込みも可能ですが、連続送信された場合は最初の１枚のみ郵送します。<br />
						※認定証の再発行は現在行っておりません。改めてクイズにチャレンジ頂くことで再発行が可能です。<br />
						※認定証に関するお問い合わせは info_lovefood@lovefood.jp までお願い致します。</p>
					</div>
					<div class="spacer20">&nbsp;</div>
					<div id="submit3">
						<a id='submitBtn' href='javascript:void(0);' class="formSend">上記内容で送信する</a>
						</form>
					</div>
				</div>
				<div class="spacer60">&nbsp;</div>
EOT;
		}
	?>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
<script src="./js/ajaxzip2/ajaxzip2.js" charset="UTF-8"></script>
</body>
</html>