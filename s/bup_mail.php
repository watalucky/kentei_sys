<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<title>福島＆会津クイズ presented by The Designium</title>
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">福島&amp;会津クイズ presented by The Designium</a></h1>
  </div>
  <div id="contents">
  	<h2>応募フォーム<br /><span id="total">ここで入力して頂いた情報を本年賀企画以外に利用することはありません。</span></h2>
		<p class="nomargin text-center"><img src="images/quiz/end.jpg"></p>
    <div class="quizArea">
    <div class="spacer20">&nbsp;</div>
<?php
require("./jphpmailer/jphpmailer.php");

//言語設定、内部エンコーディングを指定する
mb_language("japanese");
mb_internal_encoding("EUC-JP");

$name = htmlspecialchars($_POST['f_name']);
$zip = htmlspecialchars($_POST['zip']);
$pref = htmlspecialchars($_POST['pref']);
$addr = htmlspecialchars($_POST['addr']);
$f_send = htmlspecialchars($_POST['f_send']);

//日本語添付メールを送る
//宛先
$to = htmlspecialchars($_POST['f_mail']);
//題名
$subject = "会津日本酒検定証が発行されました！　".date("Y/m/d");
//本文
$body = "
	
	以下の内容で受け付けました。
	ご応募ありがとうございます。
	
	
	[住所]
	{$zip}
	{$pref}{$addr}
	
	[お名前]
	{$name}
	
	[メールアドレス]
	{$to}
	
	
	
	
	※このメールは自動返信となりますので、返信はなさらないようお願いいたします。
	
	--------------------------------------------------------
	The designium
	http://designium.com
	
	LOVEFOOD
	http://lovefood.jp
	--------------------------------------------------------
";
//差出人
$from = "info@lovefood.jp";
$bcc = "asai@thedesignium.com";
//差し出し人名
$fromname = "会津日本酒検定事務局";
//添付ファイルパス
$attachfile = "./sample.jpg";

createNinteisho($name);


$mail = new JPHPMailer();          //JPHPMailerのインスタンス生成
$mail->addTo($to);                 //宛先(To)をセット
$mail->addBcc($bcc);                 //宛先(To)をセット
$mail->setFrom($from,$fromname);   //差出人(From/From名)をセット
$mail->setSubject($subject);       //件名(Subject)をセット
$mail->setBody($body);             //本文(Body)をセット
$mail->addAttachment($attachfile); //添付ファイル追加
 
if (!$mail->send()){
    echo("メールが送信できませんでした。エラー:".$mail->getErrorMessage());
}

/*
if (!preg_match("/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/", $to)) {
    $err = '正しいメールアドレスを入力してください。';
}

else{
	

	$subject = "日本酒検定を発行します。　".date("Y/m/d");
	$body = <<<EOB
	
	以下の内容で受け付けました。
	ご応募ありがとうございます。
	
	
	[住所]
	{$zip}
	{$pref}{$addr}
	
	[お名前]
	{$name}
	
	[メールアドレス]
	{$to}
	
	
	
	
	※このメールは自動返信となりますので、返信はなさらないようお願いいたします。
	
	--------------------------------------------------------
	The designium
	http://designium.com
	
	LOVEFOOD
	http://lovefood.jp
	--------------------------------------------------------
EOB;
									
									$boundary = md5(uniqid(rand())); //バウンダリー文字（パートの境界）
	
								 // Subject部分を変換
								 $xSubject = mb_convert_encoding($subject, "utf-8", "auto");
								 $xSubject = base64_encode($xSubject);
								 $xSubject = "=?iso-2022-jp?B?".$xSubject."?=";
	
								 // Message部分を変換
									$xMessage = "This is a multi-part message in MIME format.\n\n";
									$xMessage = "--$boundary\n";
									$fp = fopen("sample.jpg", "r") or die("error");//ファイルの読み込み
							　　//$contents = fread($fp, filesize($upfile));
							　　fclose($fp);
							　　$f_encoded = chunk_split(base64_encode($contents)); //エンコードして分割
							　　$msg .= "\n\n--$boundary\n";
							　　$msg .= "Content-Type: " . $upfile_type . ";\n";
							　　$msg .= "\tname=\"$upfile_name\"\n";
							　　$msg .= "Content-Transfer-Encoding: base64\n";
							　　$msg .= "Content-Disposition: attachment;\n";
							　　$msg .= "\tfilename=\"$upfile_name\"\n\n";
							　　$msg .= "$f_encoded\n";
							　　$msg .= "--$boundary--";
								 
								 
								 $xMessage = htmlspecialchars($body);
								 $xMessage = str_replace("&amp;", "&", $xMessage);
								 if (get_magic_quotes_gpc()) $xMessage = stripslashes($xMessage);
								 $xMessage = str_replace("\r\n", "\r", $xMessage);
								 $xMessage = str_replace("\r", "\n", $xMessage);
								 $xMessage = mb_convert_encoding($xMessage, "utf-8", "auto");
	
								 // Header部分を生成
								 $GMT = date("Z");
								 $GMT_ABS  = abs($GMT);
								 $GMT_HOUR = floor($GMT_ABS / 3600);
								 $GMT_MIN = floor(($GMT_ABS - $GMT_HOUR * 3600) / 60);
								 if ($GMT >= 0) $GMT_FLG = "+"; else $GMT_FLG = "-";
								 $GMT_RFC = date("D, d M Y H:i:s ").sprintf($GMT_FLG."%02d%02d",
	$GMT_HOUR, $GMT_MIN);
	
								 $Headers  = "Date: ".$GMT_RFC."\n";
								 $Headers .= "Bcc: $bcc\n";
								 $Headers .= "From: $from\n";
								 $Headers .= "MIME-Version: 1.0\n";
								 $Headers .= "X-Mailer: PHP/".phpversion()."\n";
								 
								 //ファイル添付がある場合
								 $Headers .= "Content-Type: multipart/mixed;\n";
								 $Headers .= "\tboundary=\"$boundary\"\n";
								 
								 //$Headers .= "Content-type: text/plain; charset=utf-8\n";
								 //$Headers .= "Content-Transfer-Encoding: 7bit";
	
	
	//メール送信 終了報告
	if( mail($to,$subject,$body,$Headers) ){
		$url = "2012%e5%b9%b4%e6%96%b0%e6%98%a5%e4%bc%81%e7%94%bb%ef%bc%81%e4%bc%9a%e6%b4%a5%e3%83%bb%e7%a6%8f%e5%b3%b6%e3%81%ae%e7%be%8e%e5%91%b3%e3%81%84%e3%82%82%e3%81%ae%e5%a4%a7%e3%83%97%e3%83%ac%e3%82%bc%e3%83%b3%e3%83%88%ef%bc%81%e3%82%af%e3%82%a4%e3%82%ba%e3%81%ab%e7%ad%94%e3%81%88%e3%82%8b%e3%81%a0%e3%81%91%e3%81%ae%e7%b0%a1%e5%8d%98%e3%82%a8%e3%83%b3%e3%83%88%e3%83%aa%e3%83%bc%ef%bc%81%20http%3a%2f%2fwww%2ethedesignium%2ecom%2fnenga2012%2f";
		echo <<<EOT
      <p class="padding18">応募が完了しました。<br>入力したアドレスへ確認メールが届きますのでご確認下さい。</p>
			
EOT;
	}
	else if($err){
		echo <<<EOT
      <p class="padding18">正しいメールアドレスを入力してください。</p>
EOT;
	}
	else{
		echo <<<EOT
      <p class="padding18">申し訳ございません。メールの送信に失敗しました。時間をおいて再度やり直していただくようお願いいたします。</p>
EOT;
	}
}
*/
?>
    </div>
    <div class="spacer20">&nbsp;</div>
	</div>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
<script src="./js/ajaxzip2/ajaxzip2.js" charset="UTF-8"></script>
</body>
</html>