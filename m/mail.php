<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-6700428-29";
  $GA_PIXEL = "./ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<title>会津日本酒検定</title>
<?php
include_once("./function.php");
?>
</head>
<body id="top" text="#FFFFFF" bgcolor="#231816" style="background-color: #231816;" link="#b02100" vlink="#b02100" alink="#b02100" >
<h2 style="background-color:#b02100;color:#FFFFFF; padding: 4px 0;"><center>日本酒検定</center></h2>
<br>
<?php
mb_language("Japanese");
$to = htmlspecialchars($_POST['f_mail']);
$name = htmlspecialchars($_POST['f_name']);
$zip = htmlspecialchars($_POST['zip']);
$pref = htmlspecialchars($_POST['pref']);
$addr = htmlspecialchars($_POST['addr']);
$send = htmlspecialchars($_POST['f_send']);

//
// 認定証の生成
//
createNinteisho($_POST['f_name']);
global $imageID;


$from = "info_lovefood@lovefood.jp";
//$bcc = "fujii@thedesignium.com";
$bcc = "i-takeshi@thedesignium.com";

/*
if (!preg_match("/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/", $to)) {
    $err = '正しいメールアドレスを入力してください。';
}

else{*/

	$subject = "会津日本酒検定認定証発行";
	$body = <<<EOB
	
以下の内容で認定証発行を受け付けました。

	
[お名前]
{$name}
	
[メールアドレス]
{$to}

[認定証郵送を]
{$send}
	
[住所]
{$zip}
{$pref}{$addr}	
	
	
以下、会津日本酒検定指南役の心得をお読みになり、
日本酒の振興に努めて頂けますようお願い申し上げます。

【指南役の心得】
　一、日本酒の素晴らしさを周りに丁寧に伝えること
　一、そのためにまず自分が日本酒を深く知り、楽しむこと
　一、飲み過ぎないこと、飲ませ過ぎないこと
　一、指南役仲間を増やし、日本酒の振興に努めること

--------------------------------------------------------
　会津日本酒検定　http://lovefood.jp/sake/
　〜 一杯からの日本酒指南で美味しい酒を飲む仲間を増やそう 〜

　問題提供/監修：会津若松酒造協同組合、福島県ハイテクプラザ
　企画協力：力水會、lovefood Project
　デザイン/開発：株式会社デザイニウム
　お問い合わせ先：info_lovefood@lovefood.jp
--------------------------------------------------------
EOB;
	
	
								 // Subject部分を変換
								 $xSubject = mb_convert_encoding($subject, "JIS", "auto");
								 $xSubject = base64_encode($xSubject);
								 $xSubject = "=?iso-2022-jp?B?".$xSubject."?=";
	
								 // Message部分を変換
								 $xMessage = htmlspecialchars($body);
								 $xMessage = str_replace("&amp;", "&", $xMessage);
								 if (get_magic_quotes_gpc()) $xMessage = stripslashes($xMessage);
								 $xMessage = str_replace("\r\n", "\r", $xMessage);
								 $xMessage = str_replace("\r", "\n", $xMessage);
								 $xMessage = mb_convert_encoding($xMessage, "JIS", "auto");
	
								 // Header部分を生成
								 $GMT = date("Z");
								 $GMT_ABS  = abs($GMT);
								 $GMT_HOUR = floor($GMT_ABS / 3600);
								 $GMT_MIN = floor(($GMT_ABS - $GMT_HOUR * 3600) / 60);
								 if ($GMT >= 0) $GMT_FLG = "+"; else $GMT_FLG = "-";
								 $GMT_RFC = date("D, d M Y H:i:s ").sprintf($GMT_FLG."%02d%02d",
	$GMT_HOUR, $GMT_MIN);
	
								 $Headers  = "Date: ".$GMT_RFC."\n";
								 $Headers .= "Bcc: $bcc\n";
								 $Headers .= "From: $from\n";
								 $Headers .= "MIME-Version: 1.0\n";
								 $Headers .= "X-Mailer: PHP/".phpversion()."\n";
								 $Headers .= "Content-type: text/plain; charset=ISO-2022-JP\n";
								 $Headers .= "Content-Transfer-Encoding: 7bit";
	
	
	//メール送信 終了報告
	if( mail($to,$xSubject,$body,$Headers) ){
		$url = "2012%e5%b9%b4%e6%96%b0%e6%98%a5%e4%bc%81%e7%94%bb%ef%bc%81%e4%bc%9a%e6%b4%a5%e3%83%bb%e7%a6%8f%e5%b3%b6%e3%81%ae%e7%be%8e%e5%91%b3%e3%81%84%e3%82%82%e3%81%ae%e5%a4%a7%e3%83%97%e3%83%ac%e3%82%bc%e3%83%b3%e3%83%88%ef%bc%81%e3%82%af%e3%82%a4%e3%82%ba%e3%81%ab%e7%ad%94%e3%81%88%e3%82%8b%e3%81%a0%e3%81%91%e3%81%ae%e7%b0%a1%e5%8d%98%e3%82%a8%e3%83%b3%e3%83%88%e3%83%aa%e3%83%bc%ef%bc%81%20http%3a%2f%2fwww%2ethedesignium%2ecom%2fnenga2012%2f";
		
		$agent = $_SERVER['HTTP_USER_AGENT']; 
if(ereg("^DoCoMo", $agent)){
}else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $agent)){

}else if(ereg("^UP.Browser|^KDDI", $agent)){
	$url = "2012%94N%90V%8ft%8a%e9%89%e6%81I%89%ef%92%c3%81E%95%9f%93%87%82%cc%94%fc%96%a1%82%a2%82%e0%82%cc%91%e5%83v%83%8c%83%5b%83%93%83g%81I%83N%83C%83Y%82%c9%93%9a%82%a6%82%e9%82%be%82%af%82%cc%8a%c8%92P%83G%83%93%83g%83%8a%81%5b%81I%20http%3a%2f%2fwww%2ethedesignium%2ecom%2fnenga2012%2f";
}
		echo <<<EOT
      応募が完了しました。<br>入力したアドレスへ確認メールが届きますのでご確認下さい。<br>
EOT;
	}
	else if($err){
		echo <<<EOT
      <p class="padding18">正しいメールアドレスを入力してください。</p>
EOT;
	}
	else{/*
		echo <<<EOT
      <p class="padding18">申し訳ございません。メールの送信に失敗しました。時間をおいて再度やり直していただくようお願いいたします。</p>
EOT;*/
	}
//}
?>
	<br /><br />
	<center><font size="-2">Copyright &copy; Aizuwakamatsu Brewery Co-operative + Rikisuikai + TheDesignium Inc.</font></center><br>
</body>
</html>