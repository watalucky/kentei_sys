<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-6700428-29";
  $GA_PIXEL = "./ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<title>日本酒検定</title>
</head>

<?php

		echo <<<EOF
<body id="top" text="#FFFFFF" bgcolor="#231816" style="background-color: #231816;"><br>
<h2 style="background-color:#b02100;color:#FFFFFF; padding: 4px 0;"><center>日本酒検定</center></h2>
認定証提示サービス<br>
店舗・サービス一覧<br><br>
<table style="background-color:#ffffff;color:#231816; padding: 4px 0; width:230px; vertical-align:top;">
		<tr>
				<td style="width:35%;">参加店舗名：</td>
				<td style="color:#a00; ">居酒や ゑびす亭<br /></td>
		</tr>
		<tr>
				<td>住所：</td>
				<td>〒965-0034　会津若松市上町1-26<br /></td>
		</tr>
		<tr>
				<td>電話番号：</td>
				<td>0242-23-1318<br /></td>
		</tr>
		<tr>
				<td>ファックス：</td>
				<td>0242-23-1318　または　0242-75-4315<br /></td>
		</tr>
		<tr>
				<td  colspan="2" style="padding: 4px 4px 4px 4px;">
					<table style="background-color:#FFCC66;color:#231816; padding: 4px 4px;">
					<tr>
					<td>サービス内容<br>カード持参の方に限り、おすすめ地酒１杯200円引き<br /></td>
					</tr>
					</table>
		</td>
	</tr>		
</table><br>
<table style="background-color:#ffffff;color:#231816; padding: 4px 0; width:230px; vertical-align:top;">
		<tr>
				<td style="width:35%;">参加店舗名：</td>
				<td style="color:#a00; ">笑楽食酒　ほっぺ<br /></td>
		</tr>
		<tr>
				<td>住所：</td>
				<td>〒965-0871　会津若松市栄町7-7ステージ707ビル1F<br /></td>
		</tr>
		<tr>
				<td>電話番号：</td>
				<td>0242-22-5675<br /></td>
		</tr>
		<tr>
				<td>ファックス：</td>
				<td>0242-22-5675<br /></td>
		</tr>
		<tr>
				<td  colspan="2" style="padding: 4px 4px 4px 4px;">
					<table style="background-color:#FFCC66;color:#231816; padding: 4px 4px;">
					<tr>
					<td>サービス内容<br>おすすめ地酒　グラス小　1杯サービス、おつまみ　1品サービス<br /></td>
					</tr>
					</table>
		</td>
	</tr>		
</table><br>
<table style="background-color:#ffffff;color:#231816; padding: 4px 0; width:230px; vertical-align:top;">
		<tr>
				<td style="width:35%;">参加店舗名：</td>
				<td style="color:#a00; ">ペンション　ヴェルレーヌ<br /></td>
		</tr>
		<tr>
				<td>住所：</td>
				<td>〒969-3281　耶麻郡猪苗代町西葉山7110-20<br /></td>
		</tr>
		<tr>
				<td>電話番号：</td>
				<td>0242-63-0855<br /></td>
		</tr>
		<tr>
				<td>ファックス：</td>
				<td>0242-63-0855<br /></td>
		</tr>
		<tr>
				<td  colspan="2" style="padding: 4px 4px 4px 4px;">
					<table style="background-color:#FFCC66;color:#231816; padding: 4px 4px;">
					<tr>
					<td>サービス内容<br>カード持参の方に限り、おすすめ地酒　お猪口1杯サービス<br /></td>
					</tr>
					</table>
		</td>
	</tr>		
</table><br>
<table style="background-color:#ffffff;color:#231816; padding: 4px 0; width:230px; vertical-align:top;">
		<tr>
				<td style="width:35%;">参加店舗名：</td>
				<td style="color:#a00; ">居酒屋　よさく<br /></td>
		</tr>
		<tr>
				<td>住所：</td>
				<td>〒965-0035　会津若松市馬場町1-42<br /></td>
		</tr>
		<tr>
				<td>電話番号：</td>
				<td>0242-25-4614<br /></td>
		</tr>
		<tr>
				<td>ファックス：</td>
				<td>0242-28-8110<br /></td>
		</tr>
		<tr>
				<td  colspan="2" style="padding: 4px 4px 4px 4px;">
					<table style="background-color:#FFCC66;color:#231816; padding: 4px 4px;">
					<tr>
					<td>サービス内容<br>地酒、冷酒にてグラス約120〜150ccサービス<br /></td>
					</tr>
					</table>
		</td>
	</tr>		
</table>

	<br>
<table style="background-color:#ffffff;color:#231816; padding: 4px 0; width:230px; vertical-align:top;">
		<tr>
				<td style="width:35%;">参加店舗名：</td>
				<td style="color:#a00; ">彩喰彩酒　會津っこ<br /></td>
		</tr>
		<tr>
				<td>住所：</td>
				<td>〒965-0871　会津若松市栄町4-49<br /></td>
		</tr>
		<tr>
				<td>電話番号：</td>
				<td>0242-32-6232<br /></td>
		</tr>
		<tr>
				<td>ファックス：</td>
				<td>0242-32-6232<br /></td>
		</tr>
		<tr>
				<td  colspan="2" style="padding: 4px 4px 4px 4px;">
					<table style="background-color:#FFCC66;color:#231816; padding: 4px 4px;">
					<tr>
					<td>サービス内容<br>お猪口一杯サービス<br /></td>
					</tr>
					</table>
		</td>
	</tr>		
</table>

	<br>
	<br><center><a href="index.php">トップページに戻る</a></center><br><br>
	<center><font size="-2">Copyright &copy; Aizuwakamatsu Brewery Co-operative + Rikisuikai + TheDesignium Inc.</font></center><br>
EOF;
	
?>
</body>
</html>
