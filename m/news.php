<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-6700428-29";
  $GA_PIXEL = "./ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<title>会津日本酒検定</title>
</head>
<body id="top" text="#FFFFFF" bgcolor="#200a07" style="background-color: #200a07;">
	<img src="images/news/title.jpg" />
  <br><br>
	<table>
  	<tr>
    <td valign="top"><img src="images/news/news1.jpg" /></td>
    <td valign="top">今回の大震災によって入居する建物もかなりの損傷を受け、机の上のものは全て地面に振り落とされるほどの揺れでした。<br />
    これをきっかけにして「元々縁のない人間に地域で何が出来るのか」という元々モヤモヤしていたものが具体的に動き始めました。</td>
    </tr>
    <tr><td colspan="2"><br></td></tr>
  	<tr>
    <td valign="top"><img src="images/news/news2.jpg" /></td>
    <td valign="top">我々がITでできることと言えばウェブ。ただすぐにたくさんの支援サイトが立ち上がっており、情報＆労力が拡散するよりはと考え、地元から発信できるものとして地域の食文化を見直すという活動から地元有志とLovefood.jpをスタートしました。</td>
    </tr>
    <tr><td colspan="2"><br></td></tr>
  	<tr>
    <td valign="top"><img src="images/news/news3.jpg" /></td>
    <td valign="top">これまで弊社は会津大学のOB中心に構成されていました。<br />
    そこに会津地域のことをよく知りITの分野で働きたいという会津出身の新人、生亀(いき)と長谷川の二人が合流しました。<br />
    これは地域での雇用に対するチャレンジと考えています。</td>
    </tr>
    <tr><td colspan="2"><br></td></tr>
  	<tr>
    <td valign="top"><img src="images/news/news4.jpg" /></td>
    <td valign="top">縁あって喜多方市熱塩加納町黒岩地区に5LDKの家を借りました。住民が7人!そのうち3人が芸術家という不思議なコミュニティです。<br />
    携帯電話は通じない、でもネットがあれば仕事はできる。自然の中での体験を通じ縁を発展させる場を作りたいと考えています。</td>
    </tr>
  </table>
	<br><center><a href="index.php">トップページヘ戻る</a></center><br><br>
	<center><font size="-2">copyright TheDesignium Inc.<br>All Rights Reserved.</font></center><br>
</body>
</html>
