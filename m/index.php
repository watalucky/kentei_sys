<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-6700428-29";
  $GA_PIXEL = "./ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<title>日本酒検定</title>
</head>

<?php

		echo <<<EOF
<body id="top" text="#FFFFFF" bgcolor="#231816" style="background-color: #231816;"><br>
	<img src="images/top/h1.jpg" width="100%" /><br><br>
	検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！<br>
	<br>
	
	<blink><font color="orange">NEW</font></blink><br>
	<a href="service.php"><font color="orange">認定証でサービスが受けられる！<br>店舗リスト</font></a>
	<br><br>
	<br><center><a href="quiz.php">クイズに挑戦する!!</a></center><br><br>
	<center><font size="-2">Copyright &copy; Aizuwakamatsu Brewery Co-operative + Rikisuikai + TheDesignium Inc.</font></center><br>
EOF;
	
?>
</body>
</html>
