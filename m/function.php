<?php
//error_reporting(E_ALL);

//クイズ表示数(会津ローカルは不使用？)
define("QuizAmount", 10);
define("QuizAmount_aizuLocal", 6);

//送信画像名に使用。メール送信後にmail.phpでグローバル変数宣言して呼び出し、「$imageID.jpg」を削除している。
$imageID;

include('../question_mobile.php');

$namesArr = array(
	"sakuma" => "佐久間",
	"maeda" => "前田",
	"hata" => "秦",
	"oikawa" => "及川",
	"mizutani" => "水谷",
	"yokoyama" => "横山",
	"manabu" => "長谷川(学)",
	"erika" => "長谷川(絵)",
	"iki" => "生亀",
	"fujii" => "藤井",
	"asai" => "あさい",
	"satoh" => "佐藤"
);

//
//ランダムに問題を6つ抽出(変更可)
//
$rand_keys = array_rand($question, QuizAmount);

function quizRand(){
	global $rand_keys;
	foreach($rand_keys as $key => $val ){
		$num = $key + 1;
		quizDisp($num, $val);
	}
}

//
//トリビアを表示
//
function triviaDisp(){
	global $trivia;
	echo "<table>";
	foreach($trivia as $key => $val ){
		echo <<<EOF
			<tr>
				<td><img src="./images/trivia/{$key}.jpg"></td>
				<td class="text-white">{$val}</td>
			</tr>
EOF;
	}
	echo "</table>";
}

function quizDisp($num, $key){
	global $answer,$question,$namesArr;
	shuffle($answer[$key]);
	echo <<<EOT

<table>
<tr>
<td><img src="images/quiz/{$key}.jpg" width="40" height="40"/></td>
<td>【第{$num}問】<br /></td>
</tr>
</table><img src="images/common/sp.gif" height="5" /><br>
{$question[$key]}
<br><br>
<input type="hidden" value="無回答" name="{$key}" />
<input type="radio" value="{$answer[$key][0]}" name="{$key}" />Ａ. {$answer[$key][0]}<br />
<input type="radio" value="{$answer[$key][1]}" name="{$key}" />Ｂ. {$answer[$key][1]}<br />
<input type="radio" value="{$answer[$key][2]}" name="{$key}" />Ｃ. {$answer[$key][2]}<br />
<input type="radio" value="{$answer[$key][3]}" name="{$key}" />Ｄ. {$answer[$key][3]}<br />
<img src="images/common/sp.gif" height="15" />
<br><br>
EOT;
}

//
//認定証の生成
//
function createNinteisho($name){
	global $imageID;
	
	/*
	//MySQLに接続
	$link = mysql_connect('mysql1a.db.sakura.ne.jp', 'dsn3', 'pzhmurvfrr');
	if (!$link) {
			die('接続失敗です。'.mysql_error());
	}
	
	//データベース選択
	$db_selected = mysql_select_db('dsn3', $link);
	if (!$db_selected){
			die('データベース選択失敗です。'.mysql_error());
	}*/
	
	//MySQLに接続
	$link = mysql_connect('localhost', 'dsn', 'e2oedtxv');
	if (!$link) {
			die('接続失敗です。'.mysql_error());
	}
	
	//データベース選択
	$db_selected = mysql_select_db('dsn_syoku', $link);
	if (!$db_selected){
			die('データベース選択失敗です。'.mysql_error());
	}
	
	mysql_query('SET NAMES utf8');
	
	
	//IDを取得
	$display2 = mysql_query('
	SELECT `id`
	FROM `kentei_log`
	order by `id` desc limit 1
	');
	
	if (!$display2) {
		die('SELECTクエリーが失敗しました。'.mysql_error());
	}
	
	while ($row2 = mysql_fetch_assoc($display2)) {
		$id = $row2[id];
	}
	
	$agent = $ENV{'HTTP_USER_AGENT'};
	
	// ファイルパスを指定
	$file = "./test.jpg";
	$file2 = "./test.jpg";

	// jpg画像を作成
	$img = imagecreatefromjpeg( $file );
	$img2 = imagecreatefromjpeg( $file2 );

	// 画像サイズ取得
	$target = getimagesize( $file );
	$target2 = getimagesize( $file2 );

	// フォントサイズ
	$fsize = 36;
	$fsize2 = 56;

	// フォント角度
	$fangle = 0;

	// 位置
	// x：左からの座標
	// y：フォントペースラインの位置を指定
	$fx = 445;
	$fy = $fsize + 530;
	//$fx2 = 40;
	$fx2 = 440;
	$fy2 = $fsize2 + 520;

	// フォントカラー
	$fcolor = imagecolorallocate( $img, 0, 0, 0 );

	// フォント
	$fpath = "./../ipamp.ttf";
	$numFpath = "./../mplus-1mn-regular.ttf";

	// テキスト
	// 文字コードは『UTF-8』に変換する
	$ftext = mb_convert_encoding( $name, "UTF-8", "auto" );
	$fID = mb_convert_encoding( $id, "UTF-8", "auto" );
	$fID = 'No.'.sprintf("%04d", $fID+1);
	$imageID = $fID;
	
	// 表示用ファイル名(こちらは保存する)
	$filename = uniqid( $imageID . "_" );

	// 名前挿入(表示用)
	imagettftext(
		$img,
		$fsize2,
		$fangle,
		$fx+240,
		$fy,
		$fcolor,
		$fpath,
		$ftext
	);

	// ID挿入(表示用)
	imagettftext(
		$img,
		$fsize,
		$fangle,
		$fx,
		$fy,
		$fcolor,
		$numFpath,
		$fID
	);

	// 名前挿入(送信用)
	imagettftext(
		$img2,
		$fsize2,
		$fangle,
		$fx2+240,
		$fy2,
		$fcolor,
		$fpath,
		$ftext
	);

	// ID挿入(送信用)
	imagettftext(
		$img2,
		$fsize,
		$fangle,
		$fx2,
		$fy2,
		$fcolor,
		$numFpath,
		$fID
	);

	// 保存
	// 最後の90は品質：デフォルトは75との事
	imagejpeg( $img, "../ninteisho/" . $filename . ".jpg", 90 );
	imagejpeg( $img2, "../ninteisho/" . $imageID . ".jpg", 90 );
	
	echo <<<EOF
	<p>下記と同じ認定証がメール添付にて送信されましたのでご確認下さい。<br>数時間してもメールが届かない場合はメールアドレスが間違っているか、迷惑メールフィルターに引っかかっている可能性があります。すぐにメール到着が確認できない場合は念のため以下の画像を保存することをお奨めします</p>
	<a href='../ninteisho/{$filename}.jpg'><img src='../ninteisho/{$filename}.jpg' width='768' style="margin: 0; padding:0; text-align: center;border:3px solid #B02100;"></a><br clear='all'>
EOF;
	imagedestroy( $img );
	
	//ログ保存
	$agent = $_SERVER['HTTP_USER_AGENT'];
	
	$sql3 = "
		INSERT INTO `kentei_log` (`date`, `agent`)
		VALUES (now(), '$agent')";
	$result_flag3 = mysql_query($sql3);
	if (!$result_flag3) {
		die('3.SELECTクエリーが失敗しました。'.mysql_error());
	}
	
	//MySQL切断
	$close_flag = mysql_close($link);
}

?>