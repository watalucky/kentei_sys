<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-6700428-29";
  $GA_PIXEL = "./ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis" />
<title>日本酒検定</title>
</head>
<body id="top" text="#FFFFFF" bgcolor="#231816" style="background-color: #231816;">
<h2 style="background-color:#b02100;color:#FFFFFF; padding: 4px 0;"><center>日本酒検定</center></h2>
<br>
<form method="post" action="mailsend.php" name="gomailform">
<?php
mb_language("Japanese");
include_once("./function.php");

$missNum = 0;
$questionNum = 0; //問題数
$allowMissNum = 2; //許容間違い数

$num = 1;
foreach($_POST as $key => $val ){
	$questionNum++;
	if( ($CorrectAnswer[$key] != $val) || ($val == "") ){
		$missNum++;
	}
	//echo $val;
}
if($missNum <= $allowMissNum){
	if( $missNum == 0 ){
		$btn = "<b style=\"font-weight:bold;\"><center>全問正解で合格おめでとうございます！<br>認定証入力フォームへ進んで下さい。<br>
					<input type='submit' value='認定証入力フォームへ'></center></b><br><br>";
	}else{
		$btn = "<b style=\"font-weight:bold;\"><center>".$questionNum."問中".($questionNum-$missNum)."問で合格正解おめでとうございます！<br>認定証入力フォームへ進んで下さい。<br>
					<input type='submit' value='認定証入力フォームへ'></center></b><br><br>";
	}
}else {
	$btn = "<b style=\"font-weight:bold;\"><center>解説を読んで日本酒に少し詳しくなったら再チャレンジしましょう！<br>
					<a id='submitBtn' href='quiz.php'>再チャレンジする</a></center></b><br><br>";
}
echo $btn;

foreach($_POST as $key => $val ){
	echo "<input type=\"hidden\" name=\"hidden\" value=\"MAIL\">";
	//不正解数カウント
	if($CorrectAnswer[$key] != $val ){
		//echo $CorrectAnswer[$key]. ":" . $val;
		$result = "<font size=\"+2\" color=\"#0066FF\">残念！！</font>";
	}
	else {
		$result = "<font size=\"+2\" color=\"#FF0000\">正解！！</font>";
	}
	echo <<<EOT
	
<table>
<tr>
<td><img src="images/quiz/{$key}.jpg" width="40" height="40"/></td>
<td>【第{$num}問】<br>{$result}</td>
</tr>
</table><img src="images/common/sp.gif" height="5" /><br>
{$question[$key]}
<br><br>
<b style="font-weight:bold;">[あなたの回答]</b><br>{$val}<br>
<img src="images/common/sp.gif" height="5px"><br>
<b style="font-weight:bold;">[ただしい回答]</b><br>{$CorrectAnswer[$key]}<br>
<img src="images/common/sp.gif" height="5px"><br>
<b style="font-weight:bold;">[この問題の解説]</b><br>{$mame[$key]}<br>
<br><br>
	
EOT;
$num++;
} 
if($missNum <= $allowMissNum){
	if( $missNum == 0 ){
		$btn = "<b><center>全問正解で合格おめでとうございます！<br>認定証入力フォームへ進んで下さい。<br>
					<input type='submit' value='認定証入力フォームへ'></center></b><br><br>";
	}else{
		$btn = "<b><center>".$questionNum."問中".($questionNum-$missNum)."で合格おめでとうございます！<br>認定証入力フォームへ進んで下さい。<br>
					<input type='submit' value='認定証入力フォームへ'></center></b><br><br>";
	}
	echo $btn;
}
else {
	$btn = "<b><center>解説を読んで日本酒に少し詳しくなったら再チャレンジしましょう！<br>
					<a id='submitBtn' href='quiz.php'>再チャレンジする</a></center></b><br><br>";
	echo $btn;
}

?>
</form>
	<center><font size="-2">Copyright &copy; Aizuwakamatsu Brewery Co-operative + Rikisuikai + TheDesignium Inc.</font></center><br>
</body>
</html>
