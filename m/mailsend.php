<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-6700428-29";
  $GA_PIXEL = "./ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<title>日本酒検定</title>
</head>
<body id="top" text="#FFFFFF" bgcolor="#200a07" style="background-color: #200a07;">
<h2 style="background-color:#b02100;color:#FFFFFF; padding: 4px 0;"><center>日本酒検定</center></h2>
<br>

<?php
mb_language("Japanese");

if($_POST[hidden]){
	
?>
<br>
・名前入りの認定証を発行致しますのでお名前・メールアドレスをご入力下さい。<br />・旧字や第二水準以外の漢字は表示できない場合があります。表示がおかしい場合はご連絡ください。<br>・送信後に認定証が画面表示されると同時に記入頂いたメールアドレス宛に同じ画像が添付ファイルにて送信されます。<br>・現在試験的に名刺サイズにプリントされた認定証を郵送するサービスを行っております。希望される方は送付先住所までご記入下さい。<br>
<br>
<form method="post" id="mailFormA" name="mailFormA" action="mail.php">
          
          お名前<br>
          <input type="text" id="f_name" name="f_name" maxlength="20" /><br /><br />
          
          認定証郵送を<br>
          <label><input type="radio" id="f_send" name="f_send" value="希望する" checked />希望する</label> / <label><input type="radio" id="f_send" name="f_send" value="希望しない" />希望しない</label><br /><br />
          
          メールアドレス<br>
          <input type="text" id="f_mail" name="f_mail" style="width: 300px;" maxlength="40" /><br /><br />
          
					郵便番号<br>
          <input type="text" id="zip" name="zip"><br /><br />
          
          都道府県<br><select id="pref" name="pref">
							<option value="">選択して下さい</option>
							<option value="北海道">北海道</option><option value="青森県">青森県</option><option value="岩手県">岩手県</option><option value="宮城県">宮城県</option><option value="秋田県">秋田県</option><option value="山形県">山形県</option><option value="福島県">福島県</option><option value="茨城県">茨城県</option><option value="栃木県">栃木県</option><option value="群馬県">群馬県</option><option value="埼玉県">埼玉県</option><option value="千葉県">千葉県</option><option value="東京都">東京都</option><option value="神奈川県">神奈川県</option><option value="新潟県">新潟県</option><option value="富山県">富山県</option><option value="石川県">石川県</option><option value="福井県">福井県</option><option value="山梨県">山梨県</option><option value="長野県">長野県</option><option value="岐阜県">岐阜県</option><option value="静岡県">静岡県</option><option value="愛知県">愛知県</option><option value="三重県">三重県</option><option value="滋賀県">滋賀県</option><option value="京都府">京都府</option><option value="大阪府">大阪府</option><option value="兵庫県">兵庫県</option><option value="奈良県">奈良県</option><option value="和歌山県">和歌山県</option><option value="鳥取県">鳥取県</option><option value="島根県">島根県</option><option value="岡山県">岡山県</option><option value="広島県">広島県</option><option value="山口県">山口県</option><option value="徳島県">徳島県</option><option value="香川県">香川県</option><option value="愛媛県">愛媛県</option><option value="高知県">高知県</option><option value="福岡県">福岡県</option><option value="佐賀県">佐賀県</option><option value="長崎県">長崎県</option><option value="熊本県">熊本県</option><option value="大分県">大分県</option><option value="宮崎県">宮崎県</option><option value="鹿児島県">鹿児島県</option><option value="沖縄県">沖縄県</option></select><br><br>
              
          市区町村〜番地<br>
          <input type="text" id="addr" name="addr" style="width: 500px;" maxlength="50" /><br><br>

<strong>【注意事項】</strong><br>
※携帯電話のメールアドレスの場合、添付ファイルによる認定証が届かない場合があるためPC用のものを推奨します。<br>
※メールアドレスが不達となった場合は郵送を希望されている場合でも郵送されません。ご注意下さい。<br>
※数時間経ってもメールが届かない場合はスパムフィルターに引っかかっているか、入力ミスの可能性が高いです。<br>
※検定は何度でもチャレンジ可能ですが、フォームを連続送信された場合は最初の１枚のみ郵送します。<br>
※認定証の再発行は現在行っておりません。改めて検定にチャレンジ頂くことで再発行が可能です。<br>
※認定証に関するお問い合わせは info_lovefood@lovefood.jp までお願い致します（現在メールのみ対応です）。<br><br>

					<center><input type="submit" value="上記内容で応募する" /></center><br /><br />
</form>


<?php

}
	
?>



	<center><font size="-2">Copyright &copy; Aizuwakamatsu Brewery Co-operative + Rikisuikai + TheDesignium Inc.</font></center><br>
</body>
</html>