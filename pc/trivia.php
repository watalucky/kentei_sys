<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
 <meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<title>会津日本酒検定</title>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>


<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">日本酒検定 presented by The Designium</a></h1>
  </div>
  <div id="contents">
	<div class="baseBox">
  	<?php triviaDisp(); ?>
  </div>
  <div class="spacer15"></div>
	<a href="index.php" id="submitBtn">トップページヘ戻る!!</a>
  <div class="spacer15"></div>
	</div>

</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
