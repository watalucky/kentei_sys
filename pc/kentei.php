<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
 <meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<title>日本酒検定 presented by The Designium</title>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript"> 
history.forward();
</script> 
<?php
$csv  = array();
$file = 'csv/quizdata.csv';
$fp   = fopen($file, "r");
 
while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}
fclose($fp);
 
var_dump($csv);
?>
</head>

<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">日本酒検定 presented by The Designium</a></h1>
  </div>
  <div id="contents">
  	<h2>問題<span id="no">1</span><br /><span id="total">計<span id="QuizAmount"><?php echo QuizAmount;   define("QuizAmount", 10);
?></span>題</span></h2>
    <div class="spacer20">aaaaas&nbsp;</div>
    <form name="quizForm" action="./result.php" method="post">
    
    <?php
		quizRand();
		?>
    
    <div id="controller">
    	<a href="javascript:void(0);" id="prev">&lt;&lt;前へ</a>
    	<a href="javascript:void(0);" id="next">次へ&gt;&gt;</a>
    </div>
    
    <div id="submit">
    	<input type="submit" value="解答を提出する" id="submitBtn" />
    </div>
    
    </form>
    <input type="hidden" value="1" id="nowQuestion" name="nowQuestion" />
    <div class="spacer60">&nbsp;</div>
    
  </div>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
