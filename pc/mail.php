<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
 <meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<title>会津日本酒検定</title>
<?php
include_once("./function.php");
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="quiz">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php"><?php echo $quizinfo[1][2];?></a></h1>
  </div>
  <div id="contents">
  	<h2>指南役認定おめでとうございます。</h2>
    <div class="spacer20">&nbsp;</div>
    <div class="quizArea" style="padding:15px; width:768px;">
<?php
$to = htmlspecialchars($_POST['f_mail']);
$name = htmlspecialchars($_POST['f_name']);
$zip = htmlspecialchars($_POST['zip']);
$pref = htmlspecialchars($_POST['pref']);
$addr = htmlspecialchars($_POST['addr']);
$f_send = htmlspecialchars($_POST['f_send']);

//
// 認定証の生成
//
createNinteisho($_POST['f_name']);
global $imageID;

$from = "info_lovefood@lovefood.jp";
//// $bcc = "asai@thedesignium.com";
$bcc = "i-takeshi@thedesignium.com";


if (!preg_match("/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/", $to)) {
    $err = '正しいメールアドレスを入力してください。';
}

else{
	

	$subject = "会津日本酒検定認定証発行";
	$body = <<<EOB
	
	以下の内容で認定証発行を受け付けました。
	
	[お名前]
	{$name}
	
	[メールアドレス]
	{$to}
	
	[認定証郵送を]
	{$f_send}
	
	[住所]
	{$zip}
	{$pref}{$addr}
	
	
	以下、会津日本酒検定指南役の心得をお読みになり、
	日本酒の振興に努めて頂けますようお願い申し上げます。

	【指南役の心得】
	　一、日本酒の素晴らしさを周りに丁寧に伝えること
	　一、そのためにまず自分が日本酒を深く知り、楽しむこと
	　一、飲み過ぎないこと、飲ませ過ぎないこと
	　一、指南役仲間を増やし、日本酒の振興に努めること
	
	--------------------------------------------------------
	　会津日本酒検定　http://lovefood.jp/sake/
	　～ 一杯からの日本酒指南で美味しい酒を飲む仲間を増やそう ～
	
	　問題提供/監修：会津若松酒造協同組合、福島県ハイテクプラザ
	　企画協力：力水會、lovefood Project
	　デザイン/開発：株式会社デザイニウム
	　お問い合わせ先：info_lovefood@lovefood.jp
	--------------------------------------------------------
EOB;
	//バウンダリー文字（パートの境界）
	$boundary = "_Boundary_" . uniqid(rand(1000,9999) . '_') . "_"; 
	
	// Subject部分を変換
	$xSubject = mb_convert_encoding($subject, "ISO-2022-JP", "auto");
	$xSubject = base64_encode($xSubject);
	$xSubject = "=?iso-2022-jp?B?".$xSubject."?=";
	
	// Message部分を変換
	$xMessage = htmlspecialchars($body);
	$xMessage = str_replace("&amp;", "&", $xMessage);
	if (get_magic_quotes_gpc()) $xMessage = stripslashes($xMessage);
	$xMessage = str_replace("\r\n", "\r", $xMessage);
	$xMessage = str_replace("\r", "\n", $xMessage);
	$xMessage = mb_convert_encoding($xMessage, "ISO-2022-JP", "auto");
	
	// Header部分を生成
	$GMT = date("Z");
	$GMT_ABS  = abs($GMT);
	$GMT_HOUR = floor($GMT_ABS / 3600);
	$GMT_MIN = floor(($GMT_ABS - $GMT_HOUR * 3600) / 60);
	if ($GMT >= 0) $GMT_FLG = "+"; else $GMT_FLG = "-";
	$GMT_RFC = date("D, d M Y H:i:s ").sprintf($GMT_FLG."%02d%02d",$GMT_HOUR, $GMT_MIN);
	
	/*$Headers  = "Date: ".$GMT_RFC."\n";
	$Headers .= "Bcc: $bcc\n";
	$Headers .= "From: $from\n";
	$Headers .= "MIME-Version: 1.0\n";
	$Headers .= "X-Mailer: PHP/".phpversion()."\n";
	*/
	$Headers  = "To: $to\n" .
					 "Bcc: $bcc\n" .
					 "Date: ".$GMT_RFC."\n" .
					 "From: $from\n" .
					 "X-Mailer: PHP/" . phpversion() . "\n" .
					 "MIME-Version: 1.0\n" .
					 "Content-Type: Multipart/Mixed; boundary=\"$boundary\"\n" .
					 "Content-Transfer-Encoding: 7bit";
	
  // 添付データのエンコード
  // 日本語のファイル名はRFC違反ですが、多くのメーラは理解します
  $filename = mb_encode_mimeheader( $imageID . ".jpg" );           // ISO-2022-JP/Base64に変換
	($attach = file_get_contents("../ninteisho/" . $imageID . ".jpg")) Or die("Open Error: $filename");
  $attach   = chunk_split(base64_encode($attach),76,"\n"); // Base64に変換し76Byte分割
	
	//ファイル添付がある場合
	$body    = mb_convert_encoding($body, 'ISO-2022-JP', 'auto'); // ISO-2022-JPに変換
	// マルチパート:本文
  $mbody .= "--$boundary\n";
  $mbody .= "Content-Type: text/plain; charset=ISO-2022-JP\n" .
            "Content-Transfer-Encoding: 7bit\n";
  $mbody .= "\n";        // 空行
  $mbody .= "$body\n";   // 本文

  // マルチパート:添付ファイル
  $mbody .= "--$boundary\n";
  $mbody .= "Content-Type: $mime; name=\"$filename\"\n" .
            "Content-Transfer-Encoding: base64\n" .
            "Content-Disposition: attachment; filename=\"$filename\"\n";
  $mbody .= "\n";        // 空行
  $mbody .= "$attach\n"; // 添付

  // マルチパート:終わり
  $mbody .= "--$boundary--\n";
	
	$Headers .= "Content-type: text/plain; charset=utf-8\n";
	$Headers .= "Content-Transfer-Encoding: 7bit";
	
	
	//メール送信 終了報告
	//if( mail($to,$subject,$mbody,$Headers) ){
	if( mail($to,$xSubject,$mbody,$Headers) ){
		unlink("../ninteisho/" . $imageID . ".jpg");
		echo <<<EOT
      <!--<p class="padding18">応募が完了しました。<br>入力したアドレスへ確認メールが届きますのでご確認下さい。</p>-->
			
EOT;
	}
	else if($err){
		echo <<<EOT
      <p class="padding18">正しいメールアドレスを入力してください。</p>
EOT;
	}
	else{
		echo <<<EOT
      <p class="padding18">申し訳ございません。メールの送信に失敗しました。時間をおいて再度やり直していただくようお願いいたします。</p>
EOT;
	}
}
?>
    </div>
    <div class="spacer20">&nbsp;</div>
	</div>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
<script src="./js/ajaxzip2/ajaxzip2.js" charset="UTF-8"></script>
</body>
</html>