<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name = "viewport" content = "width = 820">
<meta name="keywords" content="日本酒検定, 会津, 会津日本酒検定, 会津若松酒造協同組合, The Designium" />
<meta name="description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！" />
<meta name="author" content="thedesignium" /> 
<meta property="og:title" content="日本酒検定 presented by The Designium" />
<meta property="og:type" content="drink" />
<meta property="og:url" content="http://lovefood.jp/sake/pc/" />
<meta property="og:image" content="http://lovefood.jp/sake/pc/images/top/thumb.gif" />
<meta property="og:site_name" content="We Love Tohoku Food" />
<meta property="fb:admins" content="100002646642678" />
 <meta property="og:description" content="検定に合格すると「会津日本酒指南役」に任命、会津若松酒造協同組合発行の『指南役認定証』が授与されます。ぜひ一合一杯からの日本酒指南を！">

<title>日本酒検定 presented by The Designium</title>
<link href="./css/common.css" rel="stylesheet" type="text/css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700428-29']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php
include_once("./function.php");
?>
</head>

<body id="service">
<div id="wrapper">
  <div id="header">
    <h1><a href="./index.php">日本酒検定 presented by The Designium</a></h1>
  </div>
  <div id="contents">
  	<h2>認定証提示サービス<br /><span id="tenpo">店舗・サービス一覧</span></h2>
    <div class="spacer20">&nbsp;</div>
    
      <div class="serviceArea">
					<table>
						<tr>
							<th>参加店舗名</th><td><strong class="text-red2">居酒や ゑびす亭</strong></td>
						</tr>
						<tr>
							<th>住所</th><td>〒965-0034　会津若松市上町1-26</td>
						</tr>
						<tr>
							<th>電話番号</th><td>0242-23-1318</td>
						</tr>
						<tr>
							<th>ファックス</th><td>0242-23-1318　または　0242-75-4315</td>
						</tr>
				</table>
						<p class="serviceLittleArea">サービス内容<br />カード持参の方に限り、おすすめ地酒１杯200円引き</p>
    </div>    
      <div class="serviceArea">
					<table>
						<tr>
							<th>参加店舗名</th><td><strong class="text-red2">笑楽食酒　ほっぺ</strong></td>
						</tr>
						<tr>
							<th>住所</th><td>〒965-0871　会津若松市栄町7-7ステージ707ビル1F</td>
						</tr>
						<tr>
							<th>電話番号</th><td>0242-22-5675</td>
						</tr>
						<tr>
							<th>ファックス</th><td>0242-22-5675</td>
						</tr>
				</table>
						<p class="serviceLittleArea">サービス内容<br />おすすめ地酒　グラス小　1杯サービス、おつまみ　1品サービス</p>
    </div>    
      <div class="serviceArea">
					<table>
						<tr>
							<th>参加店舗名</th><td><strong class="text-red2"><a class="serviceLink" href="http://www.verlai.com/" target="_blank">ペンション　ヴェルレーヌ</a></strong></td>
						</tr>
						<tr>
							<th>住所</th><td>〒969-3281　耶麻郡猪苗代町西葉山7110-20</td>
						</tr>
						<tr>
							<th>電話番号</th><td>0242-63-0855</td>
						</tr>
						<tr>
							<th>ファックス</th><td>0242-63-0855</td>
						</tr>
				</table>
						<p class="serviceLittleArea">サービス内容<br />カード持参の方に限り、おすすめ地酒　お猪口1杯サービス</p>
    </div>    
      <div class="serviceArea">
					<table>
						<tr>
							<th>参加店舗名</th><td><strong class="text-red2"><a class="serviceLink" href="http://ggyao.usen.com/0005002364/" target="_blank">居酒屋　よさく</a></strong></td>
						</tr>
						<tr>
							<th>住所</th><td>〒965-0035　会津若松市馬場町1-42</td>
						</tr>
						<tr>
							<th>電話番号</th><td>0242-25-4614</td>
						</tr>
						<tr>
							<th>ファックス</th><td>0242-28-8110</td>
						</tr>
				</table>
						<p class="serviceLittleArea">サービス内容<br />地酒、冷酒にてグラス約120～150ccサービス</p>
    </div>    
      <div class="serviceArea">
					<table>
						<tr>
							<th>参加店舗名</th><td><strong class="text-red2"><a class="serviceLink" href="http://izakaya-aizukko.com" target="_blank">彩喰彩酒　會津っこ</a></strong></td>
						</tr>
						<tr>
							<th>住所</th><td>〒965-0871　会津若松市栄町4-49</td>
						</tr>
						<tr>
							<th>電話番号</th><td>0242-32-6232</td>
						</tr>
						<tr>
							<th>ファックス</th><td>0242-32-6232</td>
						</tr>
				</table>
						<p class="serviceLittleArea">サービス内容<br />お猪口一杯サービス</p>
    </div>    
		<a href="index.php" id="submitBtn">トップページに戻る</a>
    <div class="spacer60">&nbsp;</div>
    
  </div>
</div>
<script src="http://www.google.com/jsapi"></script>
<script>
google.load("jquery", "1.6.2");
</script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
