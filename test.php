﻿<?php
$tuika = "tuika";
$msg = htmlspecialchars($_POST['msg']);
$size = htmlspecialchars($_POST['size']);
header( "Content-Type: text/html; Charset=euc-jp" );
header( "Expires: Wed, 31 May 2000 14:59:58 GMT" );
if (is_numeric($size)) {
		
	$file = "./test.jpg";

	// jpg画像を作成
	$img = imagecreatefromjpeg( $file );

	// 画像サイズ取得
	$target = getimagesize( $file );

	// フォントサイズ
	$fsize = $size;

	// フォント角度
	$fangle = 0;

	// 位置
	// x：左からの座標
	// y：フォントペースラインの位置を指定
	$fx = 40;
	$fy = $fsize + 100;

	// フォントカラー
	$fcolor = imagecolorallocate( $img, 0, 0, 0 );

	// フォント
	$fpath = "./font_BBB.OTF";

	// テキスト
	// 文字コードは『UTF-8』に変換する
	$ftext = mb_convert_encoding( $_POST['msg'], "UTF-8", "EUC-JP" );

	// テキスト挿入
	imagettftext(
		$img,
		$fsize,
		$fangle,
		$fx,
		$fy,
		$fcolor,
		$fpath,
		$ftext
	);

	// 保存
	// 最後の90は品質：デフォルトは75との事
	imagejpeg( $img, "./sample.jpg", 90 );
	echo "
	<h1>HTML埋め込みテスト</h1>
	<img src='./sample.jpg'>";
	imagedestroy( $img );
		
		
		
		
		
		
		
		
} else {
?>
<form action="test.php" method="post">
テキスト <input type="text" name="msg" value="あさい">
サイズ <input type="text" name="size" size="4" value="24">
<input type="submit" value="送信">
</form>
<?php
}
?>